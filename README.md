# foodninga

Gerald Joshua, EID: gj4593, GitLab ID: GJS153
Tianbao Zhu, EID: tz3697, GitLab ID: tianbaozhu
Zhaokun Xue, EID: zx2262, Gitlab ID: xuezhaokun
Yijin Zhao, EID: yz22566, GitLab ID: yijin98
Yikang Wang, EID: yw22279,GitLab ID: wangyikang1996

Git SHA: ceeae98cc447bcf29d3ba2787d1ffd20df372a98

project leader: Gerald Joshua

website: https://www.foodninja.club/


completion time:
Gerald Joshua: estimated: 15hr, 20hr
Tianbao Zhu: estimated: 10hr, actual: 18hr
Zhaokun Xue: estimated: 15hr, actual: 20hr
Yijin Zhao: estimated: 10hr, actual: 10hr
Yikang Wang: estimated: 10hr, 11hr
