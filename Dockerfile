FROM python:3.7-alpine
MAINTAINER FoodNinja

ENV PYTHONUNBUFFERED 1

COPY ./requirements.txt /requirements.txt
RUN pip install -r /requirements.txt

RUN mkdir /app
WORKDIR /app
COPY ./backend/app /app

RUN adduser -D user
USER user
