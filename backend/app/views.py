from django.shortcuts import render
from django.http import JsonResponse, HttpResponse
from app.models import Fruit, Vegetable, Recipe, Nutrition
from app.serializers import (
    FruitSerializer,
    VegetableSerializer,
    RecipeSerializer,
    NutritionSerializer,
)
from rest_framework import viewsets, parsers, status
from rest_framework.decorators import api_view

# Create your views here.
def index(request):
    pass


@api_view(["GET"])
def get_fruit(request):
    if request.method == "GET":
        queryset = Fruit.objects.all()
        id = request.query_params.get("id", None)
        name = request.query_params.get("name", None)

        if id is not None:
            try:
                queryset = queryset.filter(id=id)
                serializer = FruitSerializer(queryset, many=True)
                return JsonResponse(serializer.data, safe=False)
            except Fruit.DoesNotExist:
                return HttpResponse(status=status.HTTP_404_NOT_FOUND)

        elif name is not None:
            try:
                queryset = queryset.filter(name=name)
                serializer = FruitSerializer(queryset, many=True)
                return JsonResponse(serializer.data, safe=False)
            except Fruit.DoesNotExist:
                return HttpResponse(status=status.HTTP_404_NOT_FOUND)
        else:
            return HttpResponse(status=status.HTTP_404_NOT_FOUND)


@api_view(["GET"])
def all_fruit(request):
    if request.method == "GET":
        queryset = Fruit.objects.all()
        serializer = FruitSerializer(queryset, many=True)
        return JsonResponse(serializer.data, safe=False)


@api_view(["GET"])
def get_vegetable(request):
    if request.method == "GET":
        queryset = Vegetable.objects.all()
        id = request.query_params.get("id", None)
        name = request.query_params.get("name", None)

        if id is not None:
            try:
                queryset = queryset.filter(id=id)
                serializer = VegetableSerializer(queryset, many=True)
                return JsonResponse(serializer.data, safe=False)
            except Fruit.DoesNotExist:
                return HttpResponse(status=status.HTTP_404_NOT_FOUND)

        elif name is not None:
            try:
                queryset = queryset.filter(name=name)
                serializer = VegetableSerializer(queryset, many=True)
                return JsonResponse(serializer.data, safe=False)
            except Fruit.DoesNotExist:
                return HttpResponse(status=status.HTTP_404_NOT_FOUND)
        else:
            return HttpResponse(status=status.HTTP_404_NOT_FOUND)


@api_view(["GET"])
def all_vegetable(request):
    if request.method == "GET":
        queryset = Vegetable.objects.all()
        serializer = VegetableSerializer(queryset, many=True)
        return JsonResponse(serializer.data, safe=False)


@api_view(["GET"])
def get_recipe(request):
    if request.method == "GET":
        queryset = Recipe.objects.all()
        id = request.query_params.get("id", None)
        title = request.query_params.get("title", None)

        if id is not None:
            try:
                queryset = queryset.filter(id=id)
                serializer = RecipeSerializer(queryset, many=True)
                return JsonResponse(serializer.data, safe=False)
            except Fruit.DoesNotExist:
                return HttpResponse(status=status.HTTP_404_NOT_FOUND)

        elif title is not None:
            try:
                queryset = queryset.filter(title=title)
                serializer = RecipeSerializer(queryset, many=True)
                return JsonResponse(serializer.data, safe=False)
            except Fruit.DoesNotExist:
                return HttpResponse(status=status.HTTP_404_NOT_FOUND)
        else:
            return HttpResponse(status=status.HTTP_404_NOT_FOUND)


@api_view(["GET"])
def all_recipe(request):
    if request.method == "GET":
        queryset = Recipe.objects.all()
        serializer = RecipeSerializer(queryset, many=True)
        return JsonResponse(serializer.data, safe=False)


@api_view(["GET"])
def get_nutrition(request):
    if request.method == "GET":
        queryset = Nutrition.objects.all()
        id = request.query_params.get("id", None)
        name = request.query_params.get("name", None)

        if id is not None:
            try:
                queryset = queryset.filter(id=id)
                serializer = NutritionSerializer(queryset, many=True)
                return JsonResponse(serializer.data, safe=False)
            except Fruit.DoesNotExist:
                return HttpResponse(status=status.HTTP_404_NOT_FOUND)

        elif name is not None:
            try:
                queryset = queryset.filter(name=name)
                serializer = NutritionSerializer(queryset, many=True)
                return JsonResponse(serializer.data, safe=False)
            except Fruit.DoesNotExist:
                return HttpResponse(status=status.HTTP_404_NOT_FOUND)
        else:
            return HttpResponse(status=status.HTTP_404_NOT_FOUND)


@api_view(["GET"])
def all_nutrition(request):
    if request.method == "GET":
        queryset = Nutrition.objects.all()
        serializer = NutritionSerializer(queryset, many=True)
        return JsonResponse(serializer.data, safe=False)
