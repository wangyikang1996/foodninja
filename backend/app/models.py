from django.db import models

# Create your models here.
class Fruit(models.Model):

    name = models.TextField()
    category = models.TextField()
    categoryLabel = models.TextField()
    label = models.TextField()
    carbs = models.FloatField()
    fiber = models.FloatField()
    energy = models.FloatField()
    fat = models.FloatField()
    protein = models.FloatField()
    image = models.TextField(null=True)
    imageType = models.TextField(null=True)

    class Meta:
        verbose_name_plural = "fruits"


class Vegetable(models.Model):

    name = models.TextField()
    energy = models.FloatField()
    cholesterol = models.IntegerField()
    fiber = models.FloatField()
    potassium = models.FloatField()
    protein = models.FloatField()
    saturatedFat = models.FloatField()
    sodium = models.FloatField()
    sugar = models.FloatField()
    carbonhydrate = models.FloatField()
    totalFat = models.FloatField()
    servingUnit = models.TextField()
    servingWeight = models.IntegerField()
    image = models.TextField(null=True)
    imageType = models.TextField(null=True)

    class Meta:
        verbose_name_plural = "vegetables"


class Recipe(models.Model):

    likes = models.IntegerField()
    title = models.TextField()
    missedIngredientCount = models.IntegerField()
    usedIngredientCount = models.IntegerField()
    missedIngredients = models.TextField()
    usedIngredients = models.TextField()
    image = models.TextField()
    imageType = models.TextField(null=True)
    aisle = models.TextField(null=True)

    class Meta:
        verbose_name_plural = "recipes"


class Nutrition(models.Model):

    name = models.TextField()
    formula = models.TextField()  # emperical formula Cm(H2O)n
    example = models.TextField()  # C5H10O4
    type = models.TextField()  # organic or inorganic
    carbon = models.IntegerField()
    hydrogen = models.IntegerField()
    oxygen = models.IntegerField()
    other = models.TextField()
    image = models.TextField(null=True)
    imageType = models.TextField(null=True)
    description = models.TextField(null=True)

    class Meta:
        verbose_name_plural = "nutritions"
