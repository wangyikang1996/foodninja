from django.urls import path
from django.conf.urls import url
from rest_framework import routers
from . import views

# router = routers.DefaultRouter()
# router.register('api/fruit/all', views.AllFruit, 'fruits')
# urlpatterns = router.urls
urlpatterns = [
    path("", views.index, name="index"),
    path("api/fruit", views.get_fruit),
    path("api/fruit/all/", views.all_fruit),
    path("api/vegetable", views.get_vegetable),
    path("api/vegetable/all/", views.all_vegetable),
    path("api/recipe", views.get_recipe),
    path("api/recipe/all/", views.all_recipe),
    path("api/nutrition", views.get_nutrition),
    path("api/nutrition/all/", views.all_nutrition),
]
