from rest_framework import serializers
from app.models import Fruit, Vegetable, Recipe, Nutrition


class FruitSerializer(serializers.ModelSerializer):
    class Meta:
        model = Fruit
        fields = "__all__"


class VegetableSerializer(serializers.ModelSerializer):
    class Meta:
        model = Vegetable
        fields = "__all__"


class RecipeSerializer(serializers.ModelSerializer):
    class Meta:
        model = Recipe
        fields = "__all__"


class NutritionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Nutrition
        fields = "__all__"
