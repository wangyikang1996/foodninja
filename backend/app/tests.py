import unittest
import requests
import json


class TestAPI(unittest.TestCase):
    def test_fruit_by_name(self):
        url = "https://foodninja.club/api/fruit?name=Apple"
        response = requests.get(url)
        data = response.json()
        code = response.status_code
        self.assertEqual(code, 200)
        self.assertNotEqual(data, 0)
        self.assertNotEqual(data[0], 0)
        self.assertIsInstance(data, list)
        self.assertIsInstance(data[0], dict)

    def test_fruit_by_id(self):
        url = "https://foodninja.club/api/fruit?id=20"
        response = requests.get(url)
        data = response.json()
        code = response.status_code
        self.assertEqual(code, 200)
        self.assertNotEqual(data, 0)
        self.assertNotEqual(data[0], 0)
        self.assertIsInstance(data, list)
        self.assertIsInstance(data[0], dict)

    def test_fruit_all(self):
        url = "https://foodninja.club/api/fruit/all"
        response = requests.get(url)
        data = response.json()
        code = response.status_code
        self.assertEqual(code, 200)
        self.assertNotEqual(data, 0)
        self.assertNotEqual(data[0], 0)
        self.assertIsInstance(data, list)
        self.assertIsInstance(data[0], dict)

    def test_vegetable_by_name(self):
        url = "https://foodninja.club/api/vegetable?name=Beans"
        response = requests.get(url)
        data = response.json()
        code = response.status_code
        self.assertEqual(code, 200)
        self.assertNotEqual(data, 0)
        self.assertNotEqual(data[0], 0)
        self.assertIsInstance(data, list)
        self.assertIsInstance(data[0], dict)

    def test_vegetable_by_id(self):
        url = "https://foodninja.club/api/vegetable?id=35"
        response = requests.get(url)
        data = response.json()
        code = response.status_code
        self.assertEqual(code, 200)
        self.assertNotEqual(data, 0)
        self.assertNotEqual(data[0], 0)
        self.assertIsInstance(data, list)
        self.assertIsInstance(data[0], dict)

    def test_vegetable_by_id_2(self):
        url = "https://foodninja.club/api/vegetable?id=30"
        response = requests.get(url)
        data = response.json()
        code = response.status_code
        self.assertEqual(code, 200)
        self.assertNotEqual(data, 0)
        self.assertNotEqual(data[0], 0)
        self.assertIsInstance(data, list)
        self.assertIsInstance(data[0], dict)

    def test_vegetable_all(self):
        url = "https://foodninja.club/api/vegetable/all"
        response = requests.get(url)
        data = response.json()
        code = response.status_code
        self.assertEqual(code, 200)
        self.assertNotEqual(data, 0)
        self.assertNotEqual(data[0], 0)
        self.assertIsInstance(data, list)
        self.assertIsInstance(data[0], dict)

    def test_recipe_by_name(self):
        url = "https://foodninja.club/api/recipe?title=Apricot%20Shrub%20for%20Homemade%20Soda"
        response = requests.get(url)
        data = response.json()
        code = response.status_code
        self.assertEqual(code, 200)
        self.assertNotEqual(data, 0)
        self.assertNotEqual(data[0], 0)
        self.assertIsInstance(data, list)
        self.assertIsInstance(data[0], dict)

    def test_recipe_by_id(self):
        url = "https://foodninja.club/api/recipe?id=40"
        response = requests.get(url)
        data = response.json()
        code = response.status_code
        self.assertEqual(code, 200)
        self.assertNotEqual(data, 0)
        self.assertNotEqual(data[0], 0)
        self.assertIsInstance(data, list)
        self.assertIsInstance(data[0], dict)

    def test_recipe_by_id_2(self):
        url = "https://foodninja.club/api/recipe?id=2"
        response = requests.get(url)
        data = response.json()
        code = response.status_code
        self.assertEqual(code, 200)
        self.assertNotEqual(data, 0)
        self.assertNotEqual(data[0], 0)
        self.assertIsInstance(data, list)
        self.assertIsInstance(data[0], dict)

    def test_recipe_all(self):
        url = "https://foodninja.club/api/recipe/all"
        response = requests.get(url)
        data = response.json()
        code = response.status_code
        self.assertEqual(code, 200)
        self.assertNotEqual(data, 0)
        self.assertNotEqual(data[0], 0)
        self.assertIsInstance(data, list)
        self.assertIsInstance(data[0], dict)

    def test_nutrition_by_name(self):
        url = "https://foodninja.club/api/nutrition?name=protein"
        response = requests.get(url)
        data = response.json()
        code = response.status_code
        self.assertEqual(code, 200)
        self.assertNotEqual(data, 0)
        self.assertNotEqual(data[0], 0)
        self.assertIsInstance(data, list)
        self.assertIsInstance(data[0], dict)

    def test_nutrition_by_id(self):
        url = "https://foodninja.club/api/nutrition?id=3"
        response = requests.get(url)
        data = response.json()
        code = response.status_code
        self.assertEqual(code, 200)
        self.assertNotEqual(data, 0)
        self.assertNotEqual(data[0], 0)
        self.assertIsInstance(data, list)
        self.assertIsInstance(data[0], dict)

    def test_nutrition_by_id_2(self):
        url = "https://foodninja.club/api/nutrition?id=7"
        response = requests.get(url)
        data = response.json()
        code = response.status_code
        self.assertEqual(code, 200)
        self.assertNotEqual(data, 0)
        self.assertNotEqual(data[0], 0)
        self.assertIsInstance(data, list)
        self.assertIsInstance(data[0], dict)

    def test_nutrition_all(self):
        url = "https://foodninja.club/api/nutrition/all"
        response = requests.get(url)
        data = response.json()
        code = response.status_code
        self.assertEqual(code, 200)
        self.assertNotEqual(data, 0)
        self.assertNotEqual(data[0], 0)
        self.assertIsInstance(data, list)
        self.assertIsInstance(data[0], dict)


# need to add test api for nutrition model and database
if __name__ == "__main__":
    unittest.main()
