import csv


dict = {'category': u'Generic foods', 'nutrients': {u'PROCNT': 0.26, u'CHOCDF': 13.81, u'ENERC_KCAL': 52.0, u'FAT': 0.17, u'FIBTG': 2.4}, 'categoryLabel': u'food', 'foodId': u'food_a1gb9ubb72c7snbuxr3weagwv0dd', 'label': u'apple'}

with open('fruit.csv', 'w') as f:
    fieldnames = ['category','nutrients', 'categoryLabel', 'foodId', 'label']
    thewriter = csv.DictWriter(f, fieldnames = fieldnames)

    thewriter.writeheader()
    thewriter.writerow({'category': u'Generic foods', 'nutrients': {u'PROCNT': 0.26, u'CHOCDF': 13.81, u'ENERC_KCAL': 52.0, u'FAT': 0.17, u'FIBTG': 2.4}, 'categoryLabel': u'food', 'foodId': u'food_a1gb9ubb72c7snbuxr3weagwv0dd', 'label': u'apple'})
