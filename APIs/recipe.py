import requests, time, json, csv, ast

vegetables = ["Alfalfa Sprouts","Apricot","Artichoke","Asparagus","Bamboo Shoots"
,"Bean Sprouts","Beans","Beets","Belgian Endive","Bell Peppers","Bitter Melon"
,"Bok Choy","Boysenberries","Broccoflower","Broccoli","Brussels Sprouts"
,"Cabbage","Cactus Pear","Carrots","Casaba Melon"
,"Cauliflower","Celery","Chayote"
,"Collard Greens","Cucumber"
,"Eggplant","Endive","Escarole","Fennel","Garlic"
,"Green Beans","Green Onions","Greens","Hominy"
,"Horned Melon","Iceberg Lettuce","Jerusalem Artichoke","Jicama","Kale"
,"Kohlrabi","Kumquat","Leeks","Lettuce","Lima Beans"
,"Malanga","Mushrooms","Nectarines","Okra"
,"Onion","Parsnip","Peas","Peppers"
,"Pumpkin","Quince","Radicchio"
,"Radishes","Raisins","Red Cabbage","Rhubarb","Romaine Lettuce","Rutabaga"
,"Shallots","Snow Peas","Spinach","Sprouts","String Beans"
,"Tangelo"]

# fruits = ["Apple", "Apricots", "Cherries" , "Fig",
# "Grapes", "Asian Pear", "Avocado",
# "Clementine","Pear", "Banana",
# "Blackberries","Blueberries", "Passion Fruit",
# "Peaches","Pears", "Oranges","Grapefruit",
# "Pineapple","Plums","Raspberries","Strawberries",
# "Grapes","Kiwifruit","Lemons","Limes", "Dates", "Mangos","Papayas","Watermelon"
# ,"Coconuts", "Longan", "Honeydew Melon",
# "Cranberries","Gooseberries","Pomegranate", "Pitanga",
# "Plantains", "Prunes", "Prickly Pear", "Sapodilla", "Soursop",
# "Tamarind", "Mangosteen", "Olives", "Jujube Fruit", "Jackfruit", "Blackcurrant"]

def write_csv(d):
    with open('recipe.csv', 'a') as f:
        fieldnames = ['likes', 'title', 'missedIngredientCount', 'usedIngredientCount','missedIngredients','usedIngredients','id','image', 'imageType', 'aisle']
        # recipe serach by ingredients(vegetables)
        thewriter = csv.DictWriter(f, fieldnames = fieldnames)

        thewriter.writeheader()
        thewriter.writerow(d)



def recipe_api():
    counter = 1

    for ingredient in fruits:
        if counter == 10:
            counter = 1
            time.sleep(65)
        print(ingredient)


        recipe_dict = {}
        #endpoint = "https://api.spoonacular.com/recipes/search?apiKey=3400b320f7bf40a1aab3cba68b392ad8&number=1&query=" + str(id)
        endpoint = "https://api.spoonacular.com/recipes/findByIngredients?apiKey=3400b320f7bf40a1aab3cba68b392ad8&ingredients=" + str(ingredient)
        response = requests.get(endpoint)
        recipe_data = response.json()
        #print(json.dumps(recipe_data, indent=4, sort_keys=True))
        parsed = recipe_data[0]
        parsed = ast.literal_eval(json.dumps(parsed))
        missedIngredients = ""
        usedIngredients = ""
        missedIngredientCount = parsed['missedIngredientCount']
        usedIngredientCount = parsed['usedIngredientCount']

        for i in range(missedIngredientCount):
            if i == 0:
                pass
            if i == 1:
                pass
            missedIngredients += "," + parsed['missedIngredients'][i]['name']
        missedIngredients = missedIngredients[1:]

        usedIngredients =  parsed['usedIngredients'][0]['name']

        recipe_dict['likes'] = parsed['likes']
        recipe_dict['title'] = parsed['title']
        recipe_dict['missedIngredientCount'] = parsed['missedIngredientCount']
        recipe_dict['missedIngredients'] = missedIngredients
        recipe_dict['usedIngredientCount'] = parsed['usedIngredientCount']
        recipe_dict['usedIngredients'] = usedIngredients
        recipe_dict['id'] = parsed['id']
        recipe_dict['image'] = parsed['image']
        recipe_dict['imageType'] = parsed['imageType']
        recipe_dict['aisle'] = parsed['usedIngredients'][0]['aisle']

        write_csv(recipe_dict)

        counter+=1
recipe_api()
