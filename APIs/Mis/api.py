import requests
import time
import json

#lengeth 56
fruits = ["Apple", "Apricots", "Cherries" , "Fig",
"Grapes", "Asian Pear", "Avocado", "Cantaloupe", "Carambola",
"Breadfruit", "Cherimoya", "Clementine","Pear", "Banana",
"Blackberries","Blueberries", "Passion Fruit",
"Peaches","Pears", "Oranges","Grapefruit",
"Pineapple","Plums","Raspberries","Strawberries",
"Grapes","Kiwifruit","Lemons","Limes", "Dates",
"Lychee","Mangos","Papayas","Watermelon","Coconuts", "Longan", "Honeydew Melon",
"Cranberries","Gooseberries","Persimmons","Pomegranate", "Pitaya", "Pitanga",
"Plantains", "Prunes", "Prickly Pear","Pummelo", "Quience", "Sapodilla", "Soursop",
"Tamarind", "Mangosteen", "Olives", "Jujube Fruit", "Jackfruit", "Blackcurrant"]

#length 87
vegetables = ["Alfalfa Sprouts","Apricot","Artichoke","Asparagus","Bamboo Shoots"
,"Bean Sprouts","Beans","Beets","Belgian Endive","Bell Peppers","Bitter Melon"
,"Bok Choy","Boysenberries","Broccoflower","Broccoli","Brussels Sprouts"
,"Cabbage","Cactus Pear","Carrots","Casaba Melon"
,"Cauliflower","Celery","Chayote","Cherimoya"
,"Collard Greens","Corn","Cucumber"
,"Eggplant","Endive","Escarole","Fennel","Garlic"
,"Green Beans","Green Onions","Greens","Hominy"
,"Horned Melon","Iceberg Lettuce","Jerusalem Artichoke","Jicama","Kale"
,"Kohlrabi","Kumquat","Leeks","Lettuce","Lima Beans","Loquat"
,"Madarins","Malanga","Mulberries","Mushrooms","Napa","Nectarines","Okra"
,"Onion","Parsnip","Peas","Peppers"
,"Potatoes","Pumpkin","Quince","Radicchio"
,"Radishes","Raisins","Red Cabbage","Rhubarb","Romaine Lettuce","Rutabaga"
,"Shallots","Snow Peas","Spinach","Sprouts","Squash","String Beans"
,"Sweet Potato","Tangelo","Tangerines","Tomatillo","Tomato","Turnip"
,"Ugli Fruit","Water Chestnuts","Watercress","Waxed Beans","Yams"
,"Yellow Squash","Zucchini Squash"]


# # recipe serach by ingredients(vegetables)
# def recipe_api():
#     #print(endpoint)
#     for ingredient in vegetables:
#         #endpoint = "https://api.spoonacular.com/recipes/search?apiKey=3400b320f7bf40a1aab3cba68b392ad8&number=1&query=" + str(id)
#         endpoint = "https://api.spoonacular.com/recipes/findByIngredients?apiKey=3400b320f7bf40a1aab3cba68b392ad8&ingredients=" + str(ingredient)
#         response = requests.get(endpoint)
#         recipe_data = response.json()
#         print(recipe_data)
#         time.sleep(65)
#
# recipe_api()


# max 10 reqeust per minute
# getting from both nutritionix API and edmam API for VEGETABLES
# def nutrition_api():
#     headers = {'x-app-id': "46133b34",
#                 'x-app-key': "b7801bccc400b508a666fa59451a184a",
#                 'Content-Type': "application/json"}
#     url1 = "https://trackapi.nutritionix.com/v2/natural/nutrients"
#     counter = 1
#
#     for ingredient in vegetables:
#         if counter == 10:
#             counter = 1
#             time.sleep(65)
#
#         query = {"query": ingredient}
#         response = requests.post(url1, headers=headers, json=query)
#         nutrition_data = response.json()
#         print(nutrition_data['foods'])
#
#         app_id = "9c299f7e"
#         api_key = "621301a0a2757e7c876a57ea5331bddd"
#         url2 = "https://api.edamam.com/api/food-database/parser?" + "ingr=" + ingredient +"&app_id=" + app_id + "&app_key=" + api_key
#         response = requests.get(url2)
#         fruit_data = response.json()
#         print(fruit_data)
#         #time.sleep(65)
#         counter +=1
#
# nutrition_api()

# max 10 reqeust per minute
# getting from both nutritionix API and edmam API for FRUITS
def fruit_api():
    headers = {'x-app-id': "46133b34",
                'x-app-key': "b7801bccc400b508a666fa59451a184a",
                'Content-Type': "application/json"}
    url1 = "https://trackapi.nutritionix.com/v2/natural/nutrients"
    counter = 1

    for fruit in fruits:
        if counter == 2:
            counter = 1
            time.sleep(65)

        # query = {"query": fruit}
        # response = requests.post(url1, headers=headers, json=query)
        # nutrition_data = response.json()
        # print(json.dumps(nutrition_data, indent=4, sort_keys=True))
        #print(nutrition_data['foods'])

        app_id = "9c299f7e"
        api_key = "621301a0a2757e7c876a57ea5331bddd"
        url2 = "https://api.edamam.com/api/food-database/parser?" + "ingr=" + fruit +"&app_id=" + app_id + "&app_key=" + api_key
        response = requests.get(url2)
        fruit_data = response.json()
        ##print(fruit_data['hints'][0]['food'])
        print(fruit_data.keys())
        print(json.dumps(fruit_data, indent=4, sort_keys=True))
        counter += 1

fruit_api()
