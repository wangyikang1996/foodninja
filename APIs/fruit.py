import requests, time, json, csv, ast


fruits = ["Apple", "Apricots", "Cherries" , "Fig",
"Grapes", "Asian Pear", "Avocado", "Cantaloupe", "Carambola",
"Breadfruit", "Cherimoya", "Clementine","Pear", "Banana",
"Blackberries","Blueberries", "Passion Fruit",
"Peaches","Pears", "Oranges","Grapefruit",
"Pineapple","Plums","Raspberries","Strawberries",
"Grapes","Kiwifruit","Lemons","Limes", "Dates", "Mangos","Papayas","Watermelon"
,"Coconuts", "Longan", "Honeydew Melon",
"Cranberries","Gooseberries","Pomegranate", "Pitanga",
"Plantains", "Prunes", "Prickly Pear", "Sapodilla", "Soursop",
"Tamarind", "Mangosteen", "Olives", "Jujube Fruit", "Jackfruit", "Blackcurrant"]


def write_csv(d):
    with open('fruit.csv', 'a') as f:
        fieldnames = ['name','category', 'categoryLabel', 'foodId', 'label', 'Carbs', 'Fiber', 'Energy', 'Fat', 'Protein']
        thewriter = csv.DictWriter(f, fieldnames = fieldnames)

        thewriter.writeheader()
        thewriter.writerow(d)


def change_key(d, old_key, new_key):
    d[new_key] = d[old_key]
    del d[old_key]
    return d


def fruit_api():
    headers = {'x-app-id': "46133b34",
                'x-app-key': "b7801bccc400b508a666fa59451a184a",
                'Content-Type': "application/json"}
    url1 = "https://trackapi.nutritionix.com/v2/natural/nutrients"
    counter = 1

    for fruit in fruits:

        fruit_dict = {}

        if counter == 10:
            counter = 1
            time.sleep(65)

        # query = {"query": fruit}
        # response = requests.post(url1, headers=headers, json=query)
        # nutrition_data = response.json()
        # print(json.dumps(nutrition_data, indent=4, sort_keys=True))
        #print(nutrition_data['foods'])

        app_id = "9c299f7e"
        api_key = "621301a0a2757e7c876a57ea5331bddd"
        url2 = "https://api.edamam.com/api/food-database/parser?" + "ingr=" + fruit +"&app_id=" + app_id + "&app_key=" + api_key
        response = requests.get(url2)
        fruit_data = response.json()
        print(json.dumps(fruit_data, indent=4, sort_keys=True))
        parsed = fruit_data['hints'][0]['food']#['nutrients']['PROCNT']
        parsed = ast.literal_eval(json.dumps(parsed))

        for key in parsed:
            if key == 'nutrients':
                for nutrition_key in parsed[key]:
                    fruit_dict[nutrition_key] = parsed[key][nutrition_key]
            fruit_dict[key] = parsed[key]


        fruit_dict['name'] = fruit
        print(fruit)
        del fruit_dict['nutrients']

        if 'image' in fruit_dict:
            del fruit_dict['image']

        if 'FIBTG' in fruit_dict:
            fruit_dict = change_key(fruit_dict, 'FIBTG', 'Fiber')
        else:
            fruit_dict['Fiber'] = 0
        #print(fruit_data)
        fruit_dict = change_key(fruit_dict, 'PROCNT', 'Protein')
        fruit_dict = change_key(fruit_dict, 'CHOCDF', 'Carbs')
        fruit_dict = change_key(fruit_dict, 'ENERC_KCAL', 'Energy')
        fruit_dict = change_key(fruit_dict, 'FAT', 'Fat')
        write_csv(fruit_dict)
        counter += 1

fruit_api()
