import requests, time, json, csv, ast

vegetables = ["Alfalfa Sprouts","Apricot","Artichoke","Asparagus","Bamboo Shoots"
,"Bean Sprouts","Beans","Beets","Belgian Endive","Bell Peppers","Bitter Melon"
,"Bok Choy","Boysenberries","Broccoflower","Broccoli","Brussels Sprouts"
,"Cabbage","Cactus Pear","Carrots","Casaba Melon"
,"Cauliflower","Celery","Chayote","Cherimoya"
,"Collard Greens","Corn","Cucumber"
,"Eggplant","Endive","Escarole","Fennel","Garlic"
,"Green Beans","Green Onions","Greens","Hominy"
,"Horned Melon","Iceberg Lettuce","Jerusalem Artichoke","Jicama","Kale"
,"Kohlrabi","Kumquat","Leeks","Lettuce","Lima Beans","Loquat"
,"Malanga","Mushrooms","Nectarines","Okra"
,"Onion","Parsnip","Peas","Peppers"
,"Potatoes","Pumpkin","Quince","Radicchio"
,"Radishes","Raisins","Red Cabbage","Rhubarb","Romaine Lettuce","Rutabaga"
,"Shallots","Snow Peas","Spinach","Sprouts","Squash","String Beans"
,"Tangelo"]



def write_csv(d):
    with open('vegetable.csv', 'a') as f:
        fieldnames = ['name','ndb_no', 'energy', 'cholesterol', 'fiber', 'potssium',
                        'protein','saturated_fat', 'sodium',
                        'sugar','carbonhydrate', 'total_fat', 'serving_unit','serving_weight']
        thewriter = csv.DictWriter(f, fieldnames = fieldnames)

        thewriter.writeheader()
        thewriter.writerow(d)

column_list = ['ndb_no', 'nf_calories','nf_cholesterol','nf_dietary_fiber'
                ,'nf_potassium','nf_protein','nf_saturated_fat','nf_sodium'
                ,'nf_sugars','nf_total_carbohydrate','nf_total_fat', 'serving_unit', 'serving_weight_grams']

column_list_2 = ['ndb_no', 'energy', 'cholesterol', 'fiber', 'potssium',
                'protein','saturated_fat', 'sodium',
                'sugar','carbonhydrate', 'total_fat', 'serving_unit','serving_weight']

#max 10 reqeust per minute
#getting from both nutritionix API and edmam API for VEGETABLES
def nutrition_api():
    headers = {'x-app-id': "46133b34",
                'x-app-key': "b7801bccc400b508a666fa59451a184a",
                'Content-Type': "application/json"}
    url1 = "https://trackapi.nutritionix.com/v2/natural/nutrients"
    counter = 1

    for ingredient in vegetables:
        if counter == 10:
            counter = 1
            time.sleep(65)

        #print(ingredient)
        veg_dict = {}
        query = {"query": ingredient}
        response = requests.post(url1, headers=headers, json=query)
        nutrition_data = response.json()
        #print(nutrition_data['foods'])
        #print(json.dumps(nutrition_data, indent=4, sort_keys=True))
        parsed = nutrition_data['foods'][0]
        #print(type(parsed))
        #print(parsed.keys())
        veg_dict['name'] = ingredient
        for c1,c2 in zip(column_list, column_list_2):
            veg_dict[c2] = parsed[c1]

        write_csv(veg_dict)
        print(ingredient)
        #print(veg_dict)

        # app_id = "9c299f7e"
        # api_key = "621301a0a2757e7c876a57ea5331bddd"
        # url2 = "https://api.edamam.com/api/food-database/parser?" + "ingr=" + ingredient +"&app_id=" + app_id + "&app_key=" + api_key
        # response = requests.get(url2)
        # fruit_data = response.json()
        # print(fruit_data)
        counter +=1

nutrition_api()
