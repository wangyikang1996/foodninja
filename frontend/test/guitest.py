from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from unittest import main, TestCase
import time

class GUITests(TestCase):
	
	@classmethod
	def setUpClass(self):
		self.browser = webdriver.Chrome()
		time.sleep(5)

	@classmethod
	def tearDownClass(self):
		self.browser.close()

	def setUp(self):
		self.browser.get("http://localhost:3000/")
		time.sleep(3)

	def test_navbar_fruits(self):
		fruits_link = self.browser.find_element_by_link_text('Fruits')
		self.assertEqual(fruits_link.text, 'Fruits')
		fruits_link.click()
		expected_text = 'FRUIT PAGE(51 Instances)'
		time.sleep(3)
		actual_text = self.browser.find_element_by_id('model-content').text
		self.assertEqual(expected_text, actual_text)

	def test_navbar_vegetables(self):
		vegetables_link = self.browser.find_element_by_link_text('Vegetables')
		self.assertEqual(vegetables_link.text, 'Vegetables')
		vegetables_link.click()
		expected_text = 'VEGETABLE PAGE(72 Instances)'
		time.sleep(3)
		actual_text = self.browser.find_element_by_id('model-content').text
		self.assertEqual(expected_text, actual_text)


	def test_navbar_nutrition(self):
		nutrition_link = self.browser.find_element_by_link_text('Nutrition')
		self.assertEqual(nutrition_link.text, 'Nutrition')
		nutrition_link.click()
		expected_text = 'NUTRITION PAGE(9 Instances)'
		time.sleep(3)
		actual_text = self.browser.find_element_by_id('model-content').text
		self.assertEqual(expected_text, actual_text)

	def test_navbar_recipe(self):
		recipe_link = self.browser.find_element_by_link_text('Recipes')
		self.assertEqual(recipe_link.text, 'Recipes')
		recipe_link.click()
		expected_text = 'RECIPE PAGE(73 Instances)'
		time.sleep(3)
		actual_text = self.browser.find_element_by_id('model-content').text
		self.assertEqual(expected_text, actual_text)

	def test_navbar_about(self):
		about_link = self.browser.find_element_by_link_text('About')
		self.assertEqual(about_link.text, 'About')
		about_link.click()
		expected_text = 'About'
		time.sleep(3)
		actual_text = self.browser.find_element_by_id('about-title').text
		self.assertEqual(expected_text, actual_text)

	def test_navbar_search(self):
		main_search_link = self.browser.find_element_by_id('search-btn-id').text
		self.assertEqual(main_search_link, 'Search')
		main_search_link.click()
		expected_text = 'SEARCH RESULT FOR'
		time.sleep(3)
		actual_text = self.browser.find_element_by_class_name('search-result-title').text
		self.assertEqual(expected_text, actual_text)

	def test_navbar_homepage_title(self):
		homepage_title = self.browser.find_element_by_id('homepage-title').text
		self.assertEqual(homepage_title, 'Food Ninja')
		homepage_title.click()
		expected_text = 'WELCOME TO FOOD NINJA'
		time.sleep(3)
		actual_text = self.browser.find_element_by_id('welcome_title').text
		self.assertEqual(expected_text, actual_text)
		
if __name__ == '__main__':
	main()
