import request from 'request';
import chai from 'chai';

var expect = chai.expect;
describe("Webpage", function() {
	describe("MainPage", function(){
		var url = "https://foodninja.club/";

		it("returns status 200", function(done) {
	      request(url, function(error, response, body) {
	        expect(response.statusCode).to.equal(200);
	        done();
	      });
	    });

		it("No error", function(done) {
			request(url, function(error, response, body) {
				expect(error).to.equal(null);
				done();
			}
		});
		it("Loading success", function(done) {
	      request(url, function(error, response, body) {
	        expect(body).to.not.equal(null);
	        done();
	      });
    	});
	});

	describe("FruitPage", function(){
		var url = "https://foodninja.club/fruits/";

		it("No error", function(done) {
			request(url, function(error, response, body) {
				expect(error).to.equal(null);
				done();
			}
		});
		it("Loading success", function(done) {
	      request(url, function(error, response, body) {
	        expect(body).to.not.equal(null);
	        done();
	      });
    	});
	});

	describe("VegetablePage", function(){
		var url = "https://foodninja.club/vegetables/";

		it("No error", function(done) {
			request(url, function(error, response, body) {
				expect(error).to.equal(null);
				done();
			}
		});
		it("Loading success", function(done) {
	      request(url, function(error, response, body) {
	        expect(body).to.not.equal(null);
	        done();
	      });
    	});
	});

	describe("NutritionPage", function(){
		var url = "https://foodninja.club/nutrition/";

		it("No error", function(done) {
			request(url, function(error, response, body) {
				expect(error).to.equal(null);
				done();
			}
		});
		it("Loading success", function(done) {
	      request(url, function(error, response, body) {
	        expect(body).to.not.equal(null);
	        done();
	      });
    	});
	});

	describe("RecipePage", function(){
		var url = "https://foodninja.club/recipes/";

		it("No error", function(done) {
			request(url, function(error, response, body) {
				expect(error).to.equal(null);
				done();
			}
		});
		it("Loading success", function(done) {
	      request(url, function(error, response, body) {
	        expect(body).to.not.equal(null);
	        done();
	      });
    	});
	});
})