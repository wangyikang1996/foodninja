import React from 'react';
import ReactDOM from 'react-dom';
import './CSS/index.css';
import './CSS/App.css';
import App from './App';
import Fruits from './Pages/Fruits';
import Vegetables from './Pages/Vegetables';
import Nutrition from './Pages/Nutrition';
import Recipes from './Pages/Recipes';
import About from './Pages/About';
import RecipeInstance from './Pages/RecipeInstance';
import FruitInstance from './Pages/FruitInstance';
import VegetableInstance from './Pages/VegetableInstance';
import NutritionInstance from './Pages/NutritionInstance';
import NotFoundPage from './Pages/NotFoundPage';
import SearchResult from './Pages/SearchResult';
import * as serviceWorker from './serviceWorker';
import 'bootstrap/dist/css/bootstrap.min.css';
import { Redirect, BrowserRouter, Switch, Route } from 'react-router-dom';

ReactDOM.render(
	<BrowserRouter>
		<Switch>
	  		<Route exact path="/" component={App} />
	    	<Route exact path="/fruits/" component={Fruits} />
	    	<Route exact path="/fruits/:name" component={FruitInstance} />
	    	<Route exact path="/vegetables/" component={Vegetables} />
	    	<Route exact path="/vegetables/:name" component={VegetableInstance} />
	    	<Route exact path="/nutrition" component={Nutrition} />
			<Route exact path="/nutrition/:name" component={NutritionInstance} />
	    	<Route exact path="/recipes/" component={Recipes} />
	    	<Route exact path="/recipes/:name" component={RecipeInstance} />
	    	<Route exact path="/about" component={About} />
	    	<Route exact path="/main-search-result/:keyword" component={SearchResult} />
	    	<Route exact path="/main-search-result/" component={SearchResult} />
			<Route exact path="/404" component={NotFoundPage} />
    		<Redirect to="/404" />
		</Switch>
	</BrowserRouter>, 
	document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
