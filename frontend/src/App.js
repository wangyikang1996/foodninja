import React, {Component} from 'react';
import Header from './Components/Header'
import Footer from './Components/Footer'

class App extends Component {
  render(){
    return (
      <>
        <Header></Header>
        <div className="home-content">
          <div className="lead-content">
            <h1 id="welcome_title">WELCOME TO FOOD NINJA</h1>
            <h2>You are what you eat!</h2> 
            <h3>So choose your diet wisely!</h3>
          </div>
        </div>
        <Footer></Footer>
      </>
    );
  }
}

export default App;
