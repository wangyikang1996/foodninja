import React, {Component} from 'react';
import {Nav, Navbar, Form, Button} from 'react-bootstrap';
import {Link} from 'react-router-dom';

class Header extends Component {
    constructor(props) {
        super(props);
        this.state = {value: ''};
        this.handleChange = this.handleChange.bind(this);
    }

    handleChange(event) {
        this.setState({value: event.target.value});
    }

    render() {
        return(
            <>
                <Navbar bg="dark" variant="dark">
                    <Navbar.Brand id="homepage-title" href="/">Food Ninja</Navbar.Brand>
                    <Nav className="mr-auto">
                        <Nav.Link href="/fruits">Fruits</Nav.Link>
                        <Nav.Link href="/vegetables">Vegetables</Nav.Link>
                        <Nav.Link href="/nutrition">Nutrition</Nav.Link>
                        <Nav.Link href="/recipes">Recipes</Nav.Link>
                        <Nav.Link href="/about">About</Nav.Link>
                    </Nav>
                    <Form inline>
                        <Form.Control type="text" placeholder="Search" className="mr-sm-2" 
                            value={this.state.value} onChange={this.handleChange} />
                        <Link to={"/main-search-result/" + this.state.value} id="search-btn-id">
                            <Button variant="outline-info">Search</Button>
                        </Link>
                    </Form>
                </Navbar>
            </>
            );
        }

    }

export default Header;
