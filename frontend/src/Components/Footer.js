import React, {Component} from 'react';
import {Navbar} from 'react-bootstrap';
import '../CSS/App.css';

class Footer extends Component {
    render() {
        return(
            <Navbar bg="dark" variant="dark" sticky="bottom">
                <span className="footer-navbar">&copy;{new Date().getFullYear()} Food Ninja</span>
            </Navbar>
            );
        }
    }

export default Footer;
