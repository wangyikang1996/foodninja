import React, {Component} from 'react';
import {Nav, Navbar, Form, Button, Card, Row, Col} from 'react-bootstrap';
import Dropdown from 'react-bootstrap/Dropdown';
import {Link} from 'react-router-dom';
import CardDeck from 'react-bootstrap/CardDeck';
import Container from 'react-bootstrap/Container';
import Header from '../Components/Header';
import Footer from '../Components/Footer';
import Spinner from 'react-bootstrap/Spinner';
import Pagination from 'react-bootstrap/Pagination';
import PageItem from 'react-bootstrap/PageItem';
import ButtonToolbar from 'react-bootstrap/ButtonToolbar';
import Highlighter from "react-highlight-words";
import Grid from "@material-ui/core/Grid";

class Recipes extends Component {
  constructor(props) {
          super(props);
          this.state = {
            error: null,
            isLoaded: false,
            page: 0,
            lastPage: 1,
            items: [],
            modelSpinnerStatus: "model-spinner-hidden",
            filterStatus: "",
            sortBy: "",
            searchKeyword: "", 
            tempKeyword: ""
          };
          this.handleChange = this.handleChange.bind(this);
          this.searchHandleClick = this.searchHandleClick.bind(this);
  }

  componentDidMount() {
    fetch("https://www.foodninja.club/api/recipe/all/", {method: 'get'})
      .then(res => res.json())
      .then(
          (result) => {
              this.setState({
                  isLoaded: true,
                  page: 0,
                  items: result,
                  lastPage: Math.ceil(result.length/9),
                  modelSpinnerStatus: "model-spinner-hidden",
              });
          },
          (error) => {
              this.setState({
                  commits:{
                      isLoaded: true,
                      page: 0,
                      lastPage: 1,
                      modelSpinnerStatus: "model-spinner-hidden",
                      error
                  }
              });
          }
      );
    }

  handleClick(e, index) {
    
    e.preventDefault();
    let count = 0;

    this.setState({
      page: index
    });
  }

  handleChange(event) {
    this.setState({tempKeyword: event.target.value});
  }

  buttonHandleClick(e, str) {
    
    e.preventDefault();

    let { items } = this.state;
    let count = 0;
    for (const [index, value] of items.entries()) {
      	if (str == "" || str == "MissedIngredientCount0" || str == "Likes0" || str == "UsedIngredientCount0" || str == "Fat0" || str == "Protein0"
      				|| (str == "AH" && value["title"].charAt(0) >= 'A' && value["title"].charAt(0) <= 'H')
					|| (str == "IP" && value["title"].charAt(0) >= 'I' && value["title"].charAt(0) <= 'P')
					|| (str == "QZ" && value["title"].charAt(0) >= 'Q' && value["title"].charAt(0) <= 'Z')
					|| (str == "UsedIngredientCount1"  && value["usedIngredientCount"] == 1)
					|| (str == "UsedIngredientCount2" && value["usedIngredientCount"] == 2)
					|| (str == "MissedIngredientCount1"  && value["missedIngredientCount"] == 0)
					|| (str == "MissedIngredientCount2" && value["missedIngredientCount"] == 1)
					|| (str == "MissedIngredientCount3"  && value["missedIngredientCount"] >= 2)
					|| (str == "Likes1"  && value["likes"] <= 10)
					|| (str == "Likes2" && value["likes"] > 10 && value["likes"] < 100)
					|| (str == "Likes3"  && value["likes"] >= 100)){
	        count++;
	    }
    }

    this.setState({
		page: 0,
		lastPage: Math.ceil(count/9),
      	filterStatus: str,
      	modelSpinnerStatus: "model-spinner-hidden"
    });
    
  }

  searchHandleClick = () => {
  	const { items } = this.state;
    let res = this.state.tempKeyword.split(" ");
	let count = 0;

    for (const [index, value] of items.entries()) {
    	for (const word of res) {
    		if (value["title"].toUpperCase().includes(word.toUpperCase()) || value["usedIngredients"].toUpperCase().includes(word.toUpperCase())) {
    			count++;
    			break;
    		}
    	}
    }

    this.setState({
		page: 0,
		lastPage: Math.ceil(count/9),
		searchKeyword: this.state.tempKeyword,
      	modelSpinnerStatus: "model-spinner-hidden"
    });
  }

  sortButtonHandleClick(e, str) {
    e.preventDefault();
    const { items } = this.state;
    let newItemList = items
    if (str == "TitleAscending"){
        newItemList = newItemList.sort(function(a, b) {
        var nameA = a["title"].toUpperCase(); // ignore upper and lowercase
        var nameB = b["title"].toUpperCase(); // ignore upper and lowercase
        if (nameA < nameB) {
          return -1;
        }
        if (nameA > nameB) {
          return 1;
        }
        // names must be equal
        return 0;
      })
    } else if (str == "TitleDescending") {
    	newItemList = newItemList.sort(function(a, b) {
        var nameA = a["title"].toUpperCase(); // ignore upper and lowercase
        var nameB = b["title"].toUpperCase(); // ignore upper and lowercase
        if (nameA < nameB) {
          return 1;
        }
        if (nameA > nameB) {
          return -1;
        }
        // names must be equal
        return 0;
      })
	} else if (str.includes("Ascending")){
		var substr = str.substring(0, str.length - 9);
        newItemList = newItemList.sort((a, b) => a[substr] - b[substr])
    } else if (str.includes("Descending")){
    	var substr = str.substring(0, str.length - 10);
        newItemList = newItemList.sort((a, b) => -a[substr] + b[substr])
    }
    
    // newItemList = newItemList.reverse()
    this.setState({
        page: 0,
        sortBy: str,
        items: newItemList
    });
  }


  render() {
    let { error, isLoaded, page, items, modelSpinnerStatus, filterStatus, lastPage, searchKeyword, sortBy } = this.state;
    let imgList = [];
    let titleList = [];
    let usedIngredientsList = [];

    //Keyword typed by the user
    console.log(searchKeyword);
    let res = searchKeyword.split(" ");
    if (error) {
      items = null;     
    } else if (!isLoaded) {
      return (<div className="spinner-about">
        <Spinner animation="grow" size="lg" />
        </div>);
    } else {
    	if (!searchKeyword == "") {
    		for (const [index, value] of items.entries()) {
    			for (const word of res) {
		    		if (value["title"].toUpperCase().includes(word.toUpperCase()) || value["usedIngredients"].toUpperCase().includes(word.toUpperCase())) {
		    			imgList.push(value["image"]);
					    titleList.push(value["title"]);
					    usedIngredientsList.push(value["usedIngredients"])
		    			break;
		    		}
		    	}
		    }
			page = 0;
			lastPage = Math.ceil(titleList.length/9);
    	} else {
			for (const [index, value] of items.entries()) {
				if (filterStatus == "" 	|| filterStatus == "MissedIngredientCount0" || filterStatus == "Likes0" 
						|| filterStatus == "UsedIngredientCount0" || filterStatus == "Fat0" || filterStatus == "Protein0" 
						|| (filterStatus == "AH" && value["title"].charAt(0) >= 'A' && value["title"].charAt(0) <= 'H')
						|| (filterStatus == "IP" && value["title"].charAt(0) >= 'I' && value["title"].charAt(0) <= 'P')
						|| (filterStatus == "QZ" && value["title"].charAt(0) >= 'Q' && value["title"].charAt(0) <= 'Z')
						|| (filterStatus == "UsedIngredientCount1"  && value["usedIngredientCount"] == 1)
						|| (filterStatus == "UsedIngredientCount2" && value["usedIngredientCount"] == 2)
						|| (filterStatus == "MissedIngredientCount1"  && value["missedIngredientCount"] == 0)
						|| (filterStatus == "MissedIngredientCount2" && value["missedIngredientCount"] == 1)
						|| (filterStatus == "MissedIngredientCount3"  && value["missedIngredientCount"] >= 2)
						|| (filterStatus == "Likes1"  && value["likes"] < 10)
						|| (filterStatus == "Likes2" && value["likes"] >= 10 && value["likes"] < 100)
						|| (filterStatus == "Likes3"  && value["likes"] >= 100)){
			    imgList.push(value["image"]);
			    titleList.push(value["title"]);
			    if (filterStatus.includes("MissedIngredientCount") || sortBy.includes("missedIngredientCount")) {
			    	usedIngredientsList.push(value["missedIngredientCount"]);
			    } else if (filterStatus.includes("Likes") || sortBy.includes("likes")){
					usedIngredientsList.push(value["likes"]);
				} else if (filterStatus.includes("UsedIngredientCount") || sortBy.includes("usedIngredientCount")){
					usedIngredientsList.push(value["usedIngredientCount"]);
				} else {
			    	usedIngredientsList.push(value["usedIngredients"]);
			    }
			}
		}
	}

	let attribute1 = "UsedIngredients: ";
	if (filterStatus.includes("MissedIngredientCount") || sortBy.includes("missedIngredientCount")) {
		attribute1 = "MissedIngredientCount: ";
	} else if (filterStatus.includes("Likes") || sortBy.includes("likes")) {
		attribute1 = "Likes: ";
	} else if (filterStatus.includes("UsedIngredientCount") || sortBy.includes("usedIngredientCount")) {
		attribute1 = "UsedIngredientCount: ";
	}
	if (searchKeyword.length != 0){
		attribute1 = "UsedIngredients: ";
	}
	let count = page * 9;
	let range = 9;
	if (page == lastPage - 1) {
	range = titleList.length - count;
	}
	let numInstance = titleList.length;
	if (numInstance == 0){
		range = 0;
	}

	let status = "";
	if (filterStatus != "") {
		status = "/Filter by " + filterStatus.substring(0, filterStatus.length - 1);
	}
	if (sortBy != "") {
		if (sortBy.includes("Ascending")) {
			status += " /Sorted by " +  sortBy.substring(0, sortBy.length - 9) + " in ascending order";
		} else if (sortBy.includes("Descending")) {
			status += " /Sorted by " +  sortBy.substring(0, sortBy.length - 10) + " in descending order";
		}
	}

	return(
		<>
		<div className="spinner-about">
		<Spinner className={modelSpinnerStatus} animation="grow" size="lg" />
		</div>
		<Header></Header>
		<br/>
		<h2 id="model-content" className="model-title">RECIPE PAGE({numInstance} Instances){status}</h2>
		<br/>

		<Form inline className="justify-content-md-center">
            <Form.Control 
            	type="text" 
            	placeholder="Search" 
            	className="mr-sm-2" 
            	value={this.state.value} onChange={this.handleChange}
			/>
            <Button variant="outline-info" onClick={this.searchHandleClick}>Search</Button>
    	</Form>

      	<ButtonToolbar aria-label="Toolbar with button groups" className="justify-content-md-center">
      	<h2>Filter by</h2>
      		<Dropdown>
				<Dropdown.Toggle variant="success" id="dropdown-basic">
				Title
				</Dropdown.Toggle>

				<Dropdown.Menu>
					<Dropdown.Item>
						<Link onClick={e => this.buttonHandleClick(e, "AH")} href="#">
					      	{"A-H"}
					    </Link>
					</Dropdown.Item>
					<Dropdown.Item>
						<Link onClick={e => this.buttonHandleClick(e, "IP")} href="#">
					      	{"I-P"}
					    </Link>
					</Dropdown.Item>
					<Dropdown.Item>
						<Link onClick={e => this.buttonHandleClick(e, "QZ")} href="#">
					      	{"Q-Z"}
					    </Link>
					</Dropdown.Item>
					<Dropdown.Item>
						<Link onClick={e => this.buttonHandleClick(e, "")} href="#">
					      	{"All"}
					    </Link>
					</Dropdown.Item>
				</Dropdown.Menu>
			</Dropdown>

			<Dropdown>
				<Dropdown.Toggle variant="primary" id="dropdown-basic">
					UsedIngredientCount
				</Dropdown.Toggle>

				<Dropdown.Menu>
					<Dropdown.Item>
						<Link onClick={e => this.buttonHandleClick(e, "UsedIngredientCount1")} href="#">
					      	{"1"}
					    </Link>
					</Dropdown.Item>
					<Dropdown.Item>
						<Link onClick={e => this.buttonHandleClick(e, "UsedIngredientCount2")} href="#">
					      	{"2"}
					    </Link>
					</Dropdown.Item>
					<Dropdown.Item>
						<Link onClick={e => this.buttonHandleClick(e, "UsedIngredientCount0")} href="#">
					      	{"All"}
					    </Link>
					</Dropdown.Item>
				</Dropdown.Menu>
			</Dropdown>

			<Dropdown>
				<Dropdown.Toggle variant="success" id="dropdown-basic">
					MissedIngredientCount
				</Dropdown.Toggle>

				<Dropdown.Menu>
					<Dropdown.Item>
						<Link onClick={e => this.buttonHandleClick(e, "MissedIngredientCount1")} href="#">
					      	{"0"}
					    </Link>
					</Dropdown.Item>
					<Dropdown.Item>
						<Link onClick={e => this.buttonHandleClick(e, "MissedIngredientCount2")} href="#">
					      	{"1"}
					    </Link>
					</Dropdown.Item>
					<Dropdown.Item>
						<Link onClick={e => this.buttonHandleClick(e, "MissedIngredientCount3")} href="#">
					      	{">=2"}
					    </Link>
					</Dropdown.Item>
					<Dropdown.Item>
						<Link onClick={e => this.buttonHandleClick(e, "MissedIngredientCount0")} href="#">
					      	{"All"}
					    </Link>
					</Dropdown.Item>
				</Dropdown.Menu>
			</Dropdown>

			<Dropdown>
				<Dropdown.Toggle variant="primary" id="dropdown-basic">
					Likes
				</Dropdown.Toggle>

				<Dropdown.Menu>
					<Dropdown.Item>
						<Link onClick={e => this.buttonHandleClick(e, "Likes1")} href="#">
					      	{"0-10"}
					    </Link>
					</Dropdown.Item>
					<Dropdown.Item>
						<Link onClick={e => this.buttonHandleClick(e, "Likes2")} href="#">
					      	{"10-100"}
					    </Link>
					</Dropdown.Item>
					<Dropdown.Item>
						<Link onClick={e => this.buttonHandleClick(e, "Likes3")} href="#">
					      	{">100"}
					    </Link>
					</Dropdown.Item>
					<Dropdown.Item>
						<Link onClick={e => this.buttonHandleClick(e, "Likes0")} href="#">
					      	{"All"}
					    </Link>
					</Dropdown.Item>
				</Dropdown.Menu>
			</Dropdown>

      	</ButtonToolbar>

      	<ButtonToolbar aria-label="Toolbar with button groups" className="justify-content-md-center">
      		<h2>Sorted by</h2>

      		<Dropdown>
				<Dropdown.Toggle variant="primary" id="dropdown-basic">
					Title
				</Dropdown.Toggle>

				<Dropdown.Menu>
					<Dropdown.Item>
						<Link onClick={e => this.sortButtonHandleClick(e, "TitleAscending")} href="#">
					      	{"Ascending"}
					    </Link>
					</Dropdown.Item>
					<Dropdown.Item>
						<Link onClick={e => this.sortButtonHandleClick(e, "TitleDescending")} href="#">
					      	{"Descending"}
					    </Link>
					</Dropdown.Item>
				</Dropdown.Menu>
			</Dropdown>

			<Dropdown>
				<Dropdown.Toggle variant="success" id="dropdown-basic">
					UsedIngredientCount
				</Dropdown.Toggle>

				<Dropdown.Menu>
					<Dropdown.Item>
						<Link onClick={e => this.sortButtonHandleClick(e, "usedIngredientCountAscending")} href="#">
					      	{"Ascending"}
					    </Link>
					</Dropdown.Item>
					<Dropdown.Item>
						<Link onClick={e => this.sortButtonHandleClick(e, "usedIngredientCountDescending")} href="#">
					      	{"Descending"}
					    </Link>
					</Dropdown.Item>
				</Dropdown.Menu>
			</Dropdown>

			<Dropdown>
				<Dropdown.Toggle variant="primary" id="dropdown-basic">
					MissedIngredientCount
				</Dropdown.Toggle>

				<Dropdown.Menu>
					<Dropdown.Item>
						<Link onClick={e => this.sortButtonHandleClick(e, "missedIngredientCountAscending")} href="#">
					      	{"Ascending"}
					    </Link>
					</Dropdown.Item>
					<Dropdown.Item>
						<Link onClick={e => this.sortButtonHandleClick(e, "missedIngredientCountDescending")} href="#">
					      	{"Descending"}
					    </Link>
					</Dropdown.Item>
				</Dropdown.Menu>
			</Dropdown>

			<Dropdown>
				<Dropdown.Toggle variant="success" id="dropdown-basic">
					Likes
				</Dropdown.Toggle>

				<Dropdown.Menu>
					<Dropdown.Item>
						<Link onClick={e => this.sortButtonHandleClick(e, "likesAscending")} href="#">
					      	{"Ascending"}
					    </Link>
					</Dropdown.Item>
					<Dropdown.Item>
						<Link onClick={e => this.sortButtonHandleClick(e, "likesDescending")} href="#">
					      	{"Descending"}
					    </Link>
					</Dropdown.Item>
				</Dropdown.Menu>
			</Dropdown>
      	</ButtonToolbar>

      	<br/><br/>

		<Container>
		<Grid
                                container
                                spacing={9}
                                direction="row"
                                alignItems="flex-start"
                                justify="flex-start"
                                style={{ minHeight: '100vh' }}
                            >
		  	{[...Array(range)].map((page1, i) =>
		    <Grid item xs={12} sm={6} md={4} key={"Recipe"+i}>
		      <Card className="model-card">
		          <Link to={"/recipes/"+titleList[count + i]}>
		            <Card.Img className="model-card-img" variant="top" src={imgList[count + i]} height="250rem"/>
		          </Link>
		          <Card.Body>
		            <Link to={"/recipes/"+titleList[count + i]}>
		              <Card.Title className="model-instance-title">
		              <Highlighter
						    highlightClassName="YourHighlightClass"
						    searchWords={res}
						    autoEscape={true}
						    textToHighlight={titleList[count + i]}
					  />
		              </Card.Title>
		            </Link>
		            <Card.Text>{attribute1}
			            <Highlighter
							highlightClassName="YourHighlightClass"
							searchWords={res}
							autoEscape={true}
							textToHighlight={usedIngredientsList[count + i].toString()}
						/>
		            </Card.Text>
		          </Card.Body>
		      </Card>
		    </Grid>
		  )}
		</Grid>
		</Container>

      <br/><br/>

      <Pagination className="justify-content-md-center">
            <Pagination.Item disabled={page <=  0} onClick={e => this.handleClick(e, page - 1)}>
            	<b>Previous</b>
            </Pagination.Item>

            {[...Array(lastPage)].map((page1, i) => 
              	<Pagination.Item active={i === page} key={i} onClick={e => this.handleClick(e, i)}>
                  	{i + 1}
              	</Pagination.Item>
            )}

            <Pagination.Next disabled={page >= lastPage - 1} onClick={e => this.handleClick(e, page + 1)}>
                <b>Next</b>
            </Pagination.Next>
            
      </Pagination>

      <Footer></Footer>
      </>
      );
      }
    }
  }

export default Recipes;
