import React, {Component} from 'react';
import Header from '../Components/Header';
import Footer from '../Components/Footer';
import Spinner from 'react-bootstrap/Spinner';
import Jumbotron from 'react-bootstrap/Jumbotron';
import Container from 'react-bootstrap/Container';
import Card from 'react-bootstrap/Card'; //Somehow, the Card and CardDeck components can't be combined 
                                        // as one import statement. Samething with ListGroup and
                                        // ListGroupItem
import CardDeck from 'react-bootstrap/Card';
import ListGroup from 'react-bootstrap/ListGroup';
import ListGroupItem from 'react-bootstrap/ListGroup';

class About extends Component {
    constructor(props) {
        super(props);
        this.state = {
          commits: {
            error: null,
            isLoaded: false,
            items: [],
            people:{
                "yikang.wang@utexas.edu":0,
                "gerald.joshua@g.austincc.edu":0,
                "zyjqcsj@gmail.com":0,
                "tz3697@utexas.edu":0,
                "xuezhaokun@gmail.com":0    
                }
            },
          issues: {
            error: null,
            isLoaded: false,
            items: [],
            people:{
                "yikang wang":0,
                "gerald joshua":0,
                "yijin zhao":0,
                "tianbaozhu":0,
                "zhaokun xue":0
            }
          }
        };
    }

  componentDidMount() {
    fetch("https://gitlab.com/api/v4/projects/16930929/repository/commits?per_page=9999", {method: 'get'})
        .then(res => res.json())
        .then(
            (result) => {
                this.setState({
                    commits:{
                        isLoaded: true,
                        items: result,
                        people:{   
                            "yikang.wang@utexas.edu":0,
                            "gerald.joshua@g.austincc.edu":0,
                            "zyjqcsj@gmail.com":0,
                            "tz3697@utexas.edu":0,
                            "xuezhaokun@gmail.com":0 
                            }
                    }
                });
            },
            (error) => {
                this.setState.commits({
                    commits:{
                        isLoaded: true,
                        error
                    }
                });
            }
        );


   fetch('https://gitlab.com/api/v4/projects/16930929/issues?per_page=9999', {method: 'get'})
        .then(res => res.json())
        .then(
            (result) => {
                this.setState({
                    issues:{
                        isLoaded: true,
                        items: result,
                        people:{
                            "yikang wang":0,
                            "gerald joshua":0,
                            "yijin zhao":0,
                            "tianbao zhu":0,
                            "zhaokun xue":0   
                            }
                        }
                });
            },
            (error) => {
                this.setState.issues({
                    issues:{
                        isLoaded: true,
                        error
                    }
                });    
            }
        );
    }

    
    render() {
       let { commits, issues } = this.state;

        if (commits.error || issues.error) {
            if (commits.error) {
                for(let commiter in commits.people){
                    commits.people.commiter = "Error"
                }
            }

            if (issues.error) {
                for(let issuer in issues.people){
                    issues.people.issuer = "Error"
                }
            }
        } else if (!issues.isLoaded || !commits.isLoaded) {
            return (<div className="spinner-about">
                <Spinner animation="grow" size="lg" />
                </div>);
        } else {
            console.log(commits)
            console.log(issues)

            //commits
            for(let gitlab_item in commits.items){
                commits.people[commits.items[gitlab_item]["author_email"].toLowerCase()] += 1;
            }

            //Issues
            for(let gitlab_item in issues.items){
                issues.people[issues.items[gitlab_item]["author"]["name"].toLowerCase()] += 1;
            }
        }

        return(
            <>
                <Header></Header> 
                <Jumbotron fluid className="about">
                  <Container>
                    <h1 id="about-title">About</h1>
                    <Container>
                        <p className="lead text-muted">
                          Since we believe that "people are what they eat", 
                          we decided to create a website called Food Ninja that provides people with a lot of information around food such as 
                          nutrition, ingredients, etc. We know that people are busy enough with their daily activities, and hence they tend to make 
                          bad decisions on their daily consumption. We hope that through this website, people can make a better decision when choosing the right 
                          food or diet for them as well as their family and friends
                        </p>
                    </Container>
                  </Container>
                </Jumbotron>

                <Container className="team">
                    <h1 className="team-title">TEAM</h1>
                    <div className="card-deck">
                        <div className="card mb-4 box-shadow">
                            <img className="card-img-top" src={require('../IMG/Gerald.jpg')} />
                            <div className="card-body">
                                <h5 className="card-title">Gerald Joshua</h5>
                                <p className="card-text">
                                    Gerald is currently a senior-year CS student. He will graduate this May. 
                                    His main interest is in full-stack and backend development.
                                </p>
                            </div>
                            <ul class="list-group list-group-flush">
                                <li class="list-group-item">Responsibility: Front-end</li>
                                <li class="list-group-item">Commit: {commits.people["gerald.joshua@g.austincc.edu"]}</li>
                                <li class="list-group-item">Issues: {issues.people["gerald joshua"]}</li>
                                <li class="list-group-item">Tests: 4</li>
                            </ul>
                        </div>
                        <div className="card mb-4 box-shadow">
                            <img className="card-img-top" src={require('../IMG/Tianbao.png')} />
                            <div className="card-body">
                                <h5 class="card-title">Tianbao Zhu</h5>
                                <p class="card-text">
                                    Tianbao is currently a senior-year CS student. He will graduate this May. 
                                    His main interest is in backend development.
                                </p>
                            </div>
                            <ul class="list-group list-group-flush">
                                <li class="list-group-item">Responsibility: Back-end</li>
                                <li class="list-group-item">Commit: {commits.people["tz3697@utexas.edu"]}</li>
                                <li class="list-group-item">Issues: {issues.people["tianbao zhu"]}</li>
                                <li class="list-group-item">Tests: 7</li>
                            </ul>
                        </div>
                        <div className="card mb-4 box-shadow">
                            <img className="card-img-top" src={require('../IMG/Yijin.jpg')} />
                            <div className="card-body">
                                <h5 class="card-title">Yijin Zhao</h5>
                                <p class="card-text">
                                    Yijin is currently a senior-year CS student. He will graduate this May.
                                </p>
                            </div>
                            <ul class="list-group list-group-flush">
                                <li class="list-group-item">Responsibility: Front-end</li>
                                <li class="list-group-item">Commits: {commits.people["zyjqcsj@gmail.com"]}</li>
                                <li class="list-group-item">Issues: {issues.people["yijin zhao"]}</li>
                                <li class="list-group-item">Tests: 5</li>
                            </ul>
                        </div>
                    </div>
                    
                    <div class="card-deck">
                        <div className="card mb-4 box-shadow">
                            <img className="card-img-top" src={require('../IMG/Yikang.jpg')} />
                            <div className="card-body">
                                <h5 class="card-title">Yikang Wang</h5>
                                <p class="card-text">
                                    Yikang is currently a senior-year CS student. He will graduate this May. 
                                    His main interest is in full-stack and backend development.
                                </p>
                            </div>
                            <ul class="list-group list-group-flush">
                                <li class="list-group-item">Responsibility: Back-end</li>
                                <li class="list-group-item">Commit: {commits.people["yikang.wang@utexas.edu"]}</li>
                                <li class="list-group-item">Issues: {issues.people["yikang wang"]}</li>
                                <li class="list-group-item">Tests: 8</li>
                            </ul>
                        </div>
                        <div className="card mb-4 box-shadow">
                            <img className="card-img-top" src={require('../IMG/Xue.png')} />
                            <div className="card-body">
                                <h5 class="card-title">Zhaokun Xue</h5>
                                <p class="card-text">
                                    Zhaokun is a junior-year CS student. He is interested in full-stack and 
                                    front end development.
                                </p>
                            </div>
                            <ul class="list-group list-group-flush">
                                <li class="list-group-item">Responsibility: Front-end</li>
                                <li class="list-group-item">Commit: {commits.people["xuezhaokun@gmail.com"]}</li>
                                <li class="list-group-item">Issues: {issues.people["zhaokun xue"]}</li>
                                <li class="list-group-item">Tests: 4</li>
                            </ul>
                        </div>
                        <div className="card border-0">
                        </div>
                    </div>                         
                </Container>

                <Jumbotron id="about-jumbotron" className="about">
                  <Container>
                    <h1>Miscellaneous Information</h1>
                    <br />
                    <h3>Data</h3> 
                    <p className="lead">
                    <a href="https://spoonacular.com/food-api/docs" target="_blank">
                        https://spoonacular.com/food-api/docs</a><br />
                        It has information about what ingredients needed to be prepared, 
                        the step by step procedural to make the dish, 
                        as well as the user ratings for the current recipes. We called their API and
                        took the necessary information from the return JSON.<br/><br/>
                    <a href="https://www.nutritionix.com/business/api" target="_blank">
                        https://www.nutritionix.com/business/api</a><br />
                    <a href="https://www.nutritionix.com/business/api" target="_blank">
                        https://spoonacular.com/food-api/docs</a><br />
                        Both provide comprehensive information about various fruits and vegetables 
                        that commonly existed in supermarkets across the U.S as well as the nutrition 
                        about it. We called their API and took the necessary information from the return JSON.
                    </p>
                    
                    <br /><br />

                    <h3>Tools used:</h3> 
                    <p className="lead tools">
                        <div>
                            <b>Frontend:</b><br/>
                            <a href="https://reactjs.org/" target="_blank">
                                <img className="toolLogo" src={require('../IMG/reactjs.png')} />
                            </a>
                            <br/>
                            <a href="https://reactjs.org/" target="_blank">ReactJS</a>: 
                            Open-source JavaScript library used for Single Page Applications. This librar helps
                            us in splitting a website into small chunks of components that can be used
                            multiple time in different webpages. <br /><br/>
                            <a href="https://react-bootstrap.github.io/" target="_blank">
                                <img className="toolLogo" src={require('../IMG/reactbootstrap.png')} />
                            </a>
                            <br/>
                            <a href="https://react-bootstrap.github.io/" target="_blank">react-bootstrap</a>:
                            Bootstrap is a CSS Framework used for developing responsive websites. We used react-bootstrap since we 
                            were building a React app. React-bootstrap helps us in designing the layout of the website.<br/><br/>
                            <a href="https://github.com/bvaughn/react-highlight-words" target="_blank">
                                <img className="toolLogo" src={require('../IMG/react-highlight-words.png')} />
                            </a>
                            <br/>
                            <a href="https://github.com/bvaughn/react-highlight-words" target="_blank">react-highlight-words</a>:
                            react-highlight-words is a React component that highlight words within a larger body of text. We used it 
                            to highlight the keywords that our website users are looking for through our search bars.<br/><br/>
                            <a href="https://material-ui.com/" target="_blank">
                                <img className="toolLogo" src={require('../IMG/material-ui.png')} />
                            </a>
                            <br/>
                            <a href="https://material-ui.com/" target="_blank">material-ui</a>:
                            material-ui has many React components for faster and easier web development design. 
                            We used it as a complement to react-bootstrap for designing the website layout.<br/><br/>
                            <a href="https://mochajs.org/" target="_blank">
                                <img className="toolLogo" src={require('../IMG/mocha.jpeg')} />
                            </a>
                            <br/>
                            <a href="https://mochajs.org/" target="_blank">mocha</a>:
                            Mocha is a feature-rich JavaScript test framework running on Node.js and in the browser, 
                            making asynchronous testing simple. We used it for our Unit testing.<br/><br/>
                            <a href="https://www.selenium.dev/" target="_blank">
                                <img className="toolLogo" src={require('../IMG/selenium.jpg')} />
                            </a>
                            <br/>
                            <a href="https://www.selenium.dev/" target="_blank">Selenium</a>:
                            Selenium WebDriver is a web framework that permits you to execute cross-browser tests. 
                            We used it for our Acceptance testing of the GUI.<br/><br/>
                            
                            <br />






                            <b>Backend:</b> <br/>
                            <a href="https://www.postman.com/" target="_blank">
                                <img className="toolLogo" src={require('../IMG/postman.png')} />
                            </a><br/>
                            <a href="https://www.postman.com/" target="_blank">Postman</a>: 
                            A tool used for designing APIs and composes documentation about the APIs. 
                            It allows us to follow the REST architecture style and share it among the group members.<br /><br/> 
                            <a href="https://aws.amazon.com/" target="_blank">
                                <img className="toolLogo" src={require('../IMG/aws.png')} />
                            </a><br/>
                            <a href="https://aws.amazon.com/" target="_blank">AWS</a>: 
                            Cloud Computing platforms that helps us to host our website. We used Route 53
                            for rerouting, EC2 to create Linux instances to host our website application,
                            Amazon Certificate Manager for issuing an SSL certificate and in our security group, 
                            we enable port 443 to allow the public to access our site via HTTPS. We also set up a proxy 
                            ‘Nginx’ to host the static pages.<br /><br /> 
                            <a href="https://aws.amazon.com/" target="_blank">
                                <img className="toolLogo" src={require('../IMG/django.png')} />
                            </a><br/>
                            <a href="https://www.djangoproject.com/" target="_blank">Django</a>: 
                            An open source Python web framework that our backend server is using for interacting with the third-party APIs.
                            <br /><br />
                            <a href="https://docs.python.org/3/library/unittest.html" target="_blank">
                                <img className="toolLogo" src={require('../IMG/unittest.jpg')} />
                            </a><br/>
                            <a href="https://docs.python.org/3/library/unittest.html" target="_blank">unittest</a>: 
                            Python unittest supports test automation, sharing of setup and shutdown code for tests, 
                            aggregation of tests into collections, and independence of the tests from the reporting 
                            framework. We used it for testing our API's implementation<br /><br />
                        </div>
                    </p>
                    <h3>Link to Postman:</h3> 
                    <p className="lead text-muted"><a href="https://documenter.getpostman.com/view/9520013/SzKWuxVn?version=latest" target="_blank">
                    https://documenter.getpostman.com/view/9520013/SzKWuxVn?version=latest</a>
                    </p>
                    <br /><br />
                    <h3>Link to Gitlab:</h3> 
                    <p className="lead text-muted"><a href="https://gitlab.com/wangyikang1996/foodninja/" target="_blank">
                    https://gitlab.com/wangyikang1996/foodninja/</a>
                    </p>
                  </Container>
                </Jumbotron>  
                <Footer></Footer>
            </>
            );
        }
    }

export default About;
