import React, {Component} from 'react';
import Header from '../Components/Header';
import Footer from '../Components/Footer';
import Container from 'react-bootstrap/Container';

class NotFoundPage extends Component {
    render() {
        return(
            <>
		        <Header></Header>
                <Container>
                    <div className="not-found-section">
                        <h3 className="h3-not-found">404 - Not found</h3>
                    </div>
                </Container>
		        <Footer></Footer>
		     </>
            );
        }
    }

export default NotFoundPage;