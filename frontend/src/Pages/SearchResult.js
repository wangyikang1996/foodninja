import React, {Component} from 'react';
import Header from '../Components/Header';
import Footer from '../Components/Footer';
import Spinner from 'react-bootstrap/Spinner';
import Highlighter from "react-highlight-words";
import {Link} from 'react-router-dom';
import CardDeck from 'react-bootstrap/CardDeck';
import {Card} from 'react-bootstrap';
import Grid from "@material-ui/core/Grid";
import Container from 'react-bootstrap/Container';
import Dropdown from 'react-bootstrap/Dropdown'

class SearchResult extends Component {
    constructor(props) {
          super(props);
          this.state = {
            error: null,
            isLoaded: false,
            vegItems: [],
            fruitItems: [],
            recipeItems: [],
            nutritionItems: [],
            modelSpinnerStatus: "model-spinner-hidden",
            keywords: "",
            emptyKeyword: true,
            selectedModel: "All"
          };
          this.onModelClick = this.onModelClick.bind(this);
    }

    componentDidUpdate(prevProps) {
      let newString = "";
      if(this.props.match.params.keyword){
        newString = this.props.match.params.keyword.trim();
        if(newString){
          if (newString != this.state.keywords){
            this.setState({
              keywords: newString,
              emptyKeyword: false
            });
          }
        }
        
      }
    }

    componentDidMount() {
      fetch("https://www.foodninja.club/api/vegetable/all/", {method: 'get'})
      .then(res => res.json())
      .then(
          (result) => {
              this.setState({
                  isLoaded: true,
                  vegItems: result,
                  modelSpinnerStatus: "model-spinner-hidden",
                  keywords: this.props.match.params.keyword ? this.props.match.params.keyword : "Please don't put empty keyword",
                  emptyKeyword: this.props.match.params.keyword ? false : true
              });
          },
          (error) => {
              this.setState({
                  commits:{
                      isLoaded: true,
                      modelSpinnerStatus: "model-spinner-hidden",
                      error
                  }
              });
          }
      );

      fetch("https://www.foodninja.club/api/fruit/all/", {method: 'get'})
      .then(res => res.json())
      .then(
          (result) => {
              this.setState({
                  fruitItems: result,
              });
          }
      );

      fetch("https://www.foodninja.club/api/recipe/all/", {method: 'get'})
      .then(res => res.json())
      .then(
          (result) => {
              this.setState({
                  recipeItems: result,
              });
          }
      );

      fetch("https://www.foodninja.club/api/nutrition/all/", {method: 'get'})
      .then(res => res.json())
      .then(
          (result) => {
              this.setState({
                  nutritionItems: result,
              });
          }
      );
    }

    onModelClick(e, model) {
      this.setState({
          selectedModel: model
      });
    }

    render() {
        let { error, isLoaded, vegItems, fruitItems, recipeItems, nutritionItems, keywords, 
          emptyKeyword, selectedModel} = this.state;
        let vegList = [];
        let fruitList = [];
        let recipeList = [];
        let nutritionList = [];
        let keywordList = [];

        if (error) {
          return(
                  <>
                      <Header></Header>
                      <br/>
                          <h2 className="search-result-title">SEARCH RESULT FOR</h2>
                          <h5 className="keyword">"{keywords}"</h5>
                          <br /><br />
                          <Container>
                          <Grid
                              container
                              spacing={9}
                              direction="row"
                              alignItems="flex-start"
                              justify="flex-start"
                              style={{ minHeight: '100vh' }}
                          >
                          {error}
                          </Grid>
                          </Container>
                          <br /><br />
                      <Footer></Footer>
                   </>
                  );       
        } else if (!isLoaded) {
          return (<div className="spinner-about">
            <Spinner animation="grow" size="lg" />
            </div>);
        } else { 
          //console.log(vegItems);
          //console.log(fruitItems);
          //console.log(recipeItems);
          if (!emptyKeyword){
            keywordList = keywords.toLowerCase().split(/[\W_]/).sort().reverse();
              
            //Clean the keywords
            for (let j = 0; j < keywordList.length; j++){
              keywordList[j] = keywordList[j].trim();
            }

            if(selectedModel == "All" || selectedModel == "Vegetable"){
              //Find the veg that contains the interested keyword
              for (let i = 0; i < vegItems.length; i++) {
                for (let j = 0; j < keywordList.length; j++){
                  var curKeyword = keywordList[j].toLowerCase().trim();
                  var curVegTitle = vegItems[i]["name"].toLowerCase().trim();
                  var curVegUnit = vegItems[i]["servingUnit"].toLowerCase().trim();
                  if(curVegTitle.includes(curKeyword) || curVegUnit.includes(curKeyword)){
                    vegList.push(vegItems[i]);
                    break;
                  }
                }
              }
            }
            
            if(selectedModel == "All" || selectedModel == "Fruit"){
              //Find the fruit that contains the interested keyword
              for (let i = 0; i < fruitItems.length; i++) {
                for (let j = 0; j < keywordList.length; j++){
                  var curKeyword = keywordList[j].toLowerCase().trim();
                  var curFruitTitle = fruitItems[i]["name"].toLowerCase().trim();
                  var curFruitCateogry = fruitItems[i]["category"].toLowerCase().trim();
                  if(curFruitTitle.includes(curKeyword) || curFruitCateogry.includes(curKeyword)){
                    fruitList.push(fruitItems[i]);
                    break;
                  }
                }
              }
            }

            if(selectedModel == "All" || selectedModel == "Recipe"){
              //Find the recipe that contains the interested keyword
              for (let i = 0; i < recipeItems.length; i++) {
                for (let j = 0; j < keywordList.length; j++){
                  var curKeyword = keywordList[j].toLowerCase().trim();
                  var curRecipeTitle = recipeItems[i]["title"].toLowerCase().trim();
                  var curRecipeIng = (recipeItems[i]["usedIngredients"] + "," + recipeItems[i]["missedIngredients"]
                    ).toLowerCase().trim();
                  var curRecipeAisle = recipeItems[i]["aisle"].toLowerCase().trim();
                  if(curRecipeTitle.includes(curKeyword) || curRecipeIng.includes(curKeyword) ||
                    curRecipeAisle.includes(curKeyword)){
                    recipeList.push(recipeItems[i]);
                    break;
                  }
                }
              }
            }

            if(selectedModel == "All" || selectedModel == "Nutrition"){
              //Find the nutrition that contains the interested keyword
              for (let i = 0; i < nutritionItems.length; i++) {
                for (let j = 0; j < keywordList.length; j++){
                  var curKeyword = keywordList[j].toLowerCase().trim();
                  var curNutritionTitle = nutritionItems[i]["name"].toLowerCase().trim();
                  var curNutritionFormula = nutritionItems[i]["formula"].toLowerCase().trim();
                  var curNutritionEx = nutritionItems[i]["example"].toLowerCase().trim();
                  if(curNutritionTitle.includes(curKeyword) || curNutritionFormula.includes(curKeyword) ||
                    curNutritionEx.includes(curKeyword)){
                    nutritionList.push(nutritionItems[i]);
                    break;
                  }
                }
              }
            }
          }

          //console.log(nutritionList);

          return(
                  <>
                      <Header></Header>
                      <br/>
                          <h2 className="search-result-title">SEARCH RESULT FOR</h2>
                          <h1 className="keyword"><h1>"{keywords}"</h1></h1>
                          <br />
                          <Container>
                            <div className="keyword-dropdown">
                              <h4 className="keyword"><h4>Select the instance model</h4></h4>
                              <Dropdown>
                                <Dropdown.Toggle variant="secondary" id="dropdown-basic">
                                  {this.state.selectedModel}
                                </Dropdown.Toggle>
                  
                                <Dropdown.Menu>
                                  <Dropdown.Item onClick={e => this.onModelClick(e, "Fruit")} href="#">Fruit</Dropdown.Item>
                                  <Dropdown.Item onClick={e => this.onModelClick(e, "Vegetable")} href="#">Vegetable</Dropdown.Item>
                                  <Dropdown.Item onClick={e => this.onModelClick(e, "Nutrition")} href="#">Nutrition</Dropdown.Item>
                                  <Dropdown.Item onClick={e => this.onModelClick(e, "Recipe")} href="#">Recipe</Dropdown.Item>
                                  <Dropdown.Item onClick={e => this.onModelClick(e, "All")} href="#">All</Dropdown.Item>
                                </Dropdown.Menu>
                              </Dropdown>
                            </div>
                            <br /><br />
                            <Grid
                                container
                                spacing={9}
                                direction="row"
                                alignItems="flex-start"
                                justify="flex-start"
                                style={{ minHeight: '100vh' }}
                            >
                              {vegList.map((veg, idx) => (
                                <Grid item xs={12} sm={6} md={4} key={"veg"+idx}>
                                  <Card className="model-card">
                                      <Link to={"/vegetables/"+veg["name"]}>
                                        <Card.Img className="model-card-img" variant="top" src={veg["image"]} height="250rem"/>
                                      </Link>
                                      <Card.Body>
                                        <Link to={"/vegetables/"+veg["name"]}>
                                          <Card.Title className="model-instance-title">
                                          <Highlighter
                                            highlightClassName="keyword-highlight"
                                            searchWords={keywordList}
                                            autoEscape={true}
                                            textToHighlight={veg["name"]}
                                          />
                                          </Card.Title>
                                        </Link>
                                        <Card.Text>Serving Unit:
                                          <Highlighter
                                            highlightClassName="keyword-highlight"
                                            searchWords={keywordList}
                                            autoEscape={true}
                                            textToHighlight={veg["servingUnit"]}
                                          />
                                        </Card.Text>
                                      </Card.Body>
                                  </Card>
                                </Grid> 
                                )
                              )}

                              {fruitList.map((fruit, idx) => (
                                <Grid item xs={12} sm={6} md={4} key={"fruit"+idx}>
                                  <Card className="model-card">
                                      <Link to={"/fruits/"+fruit["name"]}>
                                        <Card.Img className="model-card-img" variant="top" src={fruit["image"]} height="250rem"/>
                                      </Link>
                                      <Card.Body>
                                        <Link to={"/fruits/"+fruit["name"]}>
                                          <Card.Title className="model-instance-title">
                                          <Highlighter
                                            highlightClassName="keyword-highlight"
                                            searchWords={keywordList}
                                            autoEscape={true}
                                            textToHighlight={fruit["name"]}
                                          />
                                          </Card.Title>
                                        </Link>
                                        <Card.Text>Category:
                                          <Highlighter
                                            highlightClassName="keyword-highlight"
                                            searchWords={keywordList}
                                            autoEscape={true}
                                            textToHighlight={fruit["category"]}
                                          />
                                        </Card.Text>
                                      </Card.Body>
                                  </Card>
                                </Grid> 
                                )
                              )}

                              {recipeList.map((recipe, idx) => (
                                <Grid item xs={12} sm={6} md={4} key={"recipe"+idx}>
                                  <Card className="model-card">
                                      <Link to={"/recipes/"+recipe["title"]}>
                                        <Card.Img className="model-card-img" variant="top" src={recipe["image"]} height="250rem"/>
                                      </Link>
                                      <Card.Body>
                                        <Link to={"/recipes/"+recipe["title"]}>
                                          <Card.Title className="model-instance-title">
                                          <Highlighter
                                            highlightClassName="keyword-highlight"
                                            searchWords={keywordList}
                                            autoEscape={true}
                                            textToHighlight={recipe["title"]}
                                          />
                                          </Card.Title>
                                        </Link>
                                        <Card.Text>Ingredients:
                                          <Highlighter
                                            highlightClassName="keyword-highlight"
                                            searchWords={keywordList}
                                            autoEscape={true}
                                            textToHighlight={recipe["usedIngredients"]+","+recipe["missedIngredients"]}
                                          />
                                        </Card.Text>
                                        <Card.Text>Aisle:
                                          <Highlighter
                                            highlightClassName="keyword-highlight"
                                            searchWords={keywordList}
                                            autoEscape={true}
                                            textToHighlight={recipe["aisle"]}
                                          />
                                        </Card.Text>
                                      </Card.Body>
                                  </Card>
                                </Grid> 
                                )
                              )}

                              {nutritionList.map((nutrition, idx) => (
                                <Grid item xs={12} sm={6} md={4} key={"nutrition"+idx}>
                                  <Card className="model-card">
                                      <Link to={"/nutrition/"+nutrition["name"]}>
                                        <Card.Img className="model-card-img" variant="top" src={nutrition["image"]} height="250rem"/>
                                      </Link>
                                      <Card.Body>
                                        <Link to={"/nutrition/"+nutrition["name"]}>
                                          <Card.Title className="model-instance-title">
                                          <Highlighter
                                            highlightClassName="keyword-highlight"
                                            searchWords={keywordList}
                                            autoEscape={true}
                                            textToHighlight={nutrition["name"]}
                                          />
                                          </Card.Title>
                                        </Link>
                                        <Card.Text>Formula:
                                          <Highlighter
                                            highlightClassName="keyword-highlight"
                                            searchWords={keywordList}
                                            autoEscape={true}
                                            textToHighlight={nutrition["formula"]}
                                          />
                                        </Card.Text>
                                        <Card.Text>Example:
                                          <Highlighter
                                            highlightClassName="keyword-highlight"
                                            searchWords={keywordList}
                                            autoEscape={true}
                                            textToHighlight={nutrition["example"]}
                                          />
                                        </Card.Text>
                                      </Card.Body>
                                  </Card>
                                </Grid> 
                                )
                              )}

                          </Grid>
                        </Container>
                      <br/><br/>
                      <Footer></Footer>
                   </>
                  );
                }
          }
    }

export default SearchResult;