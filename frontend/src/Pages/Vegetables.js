import React, {Component} from 'react';
import {Nav, Navbar, Form, Button, Card, Row, Col} from 'react-bootstrap';
import Dropdown from 'react-bootstrap/Dropdown';
import {Link} from 'react-router-dom';
import CardDeck from 'react-bootstrap/CardDeck';
import Container from 'react-bootstrap/Container';
import Header from '../Components/Header';
import Footer from '../Components/Footer';
import Spinner from 'react-bootstrap/Spinner';
import Pagination from 'react-bootstrap/Pagination';
import PageItem from 'react-bootstrap/PageItem';
import ButtonToolbar from 'react-bootstrap/ButtonToolbar';
import Highlighter from "react-highlight-words";
import Grid from "@material-ui/core/Grid";

class Vegetables extends Component {
  constructor(props) {
          super(props);
          this.state = {
            error: null,
            isLoaded: false,
            page: 0,
            lastPage: 1,
            items: [],
            modelSpinnerStatus: "model-spinner-hidden",
            filterStatus: "",
            sortBy: "",
            searchKeyword: "", 
            tempKeyword: ""
          };
          this.handleChange = this.handleChange.bind(this);
          this.searchHandleClick = this.searchHandleClick.bind(this);
  }

  componentDidMount() {
    fetch("https://www.foodninja.club/api/vegetable/all/", {method: 'get'})
      .then(res => res.json())
      .then(
          (result) => {
              this.setState({
                  isLoaded: true,
                  page: 0,
                  items: result,
                  lastPage: Math.ceil(result.length/9),
                  modelSpinnerStatus: "model-spinner-hidden",
              });
          },
          (error) => {
              this.setState({
                  commits:{
                      isLoaded: true,
                      page: 0,
                      lastPage: 1,
                      modelSpinnerStatus: "model-spinner-hidden",
                      error
                  }
              });
          }
      );
    }

  handleClick(e, index) {
    
    e.preventDefault();
    let count = 0;

    this.setState({
      page: index
    });
  }

  handleChange(event) {
    this.setState({tempKeyword: event.target.value});
  }

  buttonHandleClick(e, str) {
    
    e.preventDefault();

    let { items } = this.state;
    let count = 0;
    for (const [index, value] of items.entries()) {
      	if (str == "" || str == "Carbonhydrate0" || str == "Fiber0" || str == "Energy0" || str == "Sodium0" || str == "Potassium0"
      				|| (str == "AH" && value["name"].charAt(0) >= 'A' && value["name"].charAt(0) <= 'H')
					|| (str == "IP" && value["name"].charAt(0) >= 'I' && value["name"].charAt(0) <= 'P')
					|| (str == "QZ" && value["name"].charAt(0) >= 'Q' && value["name"].charAt(0) <= 'Z')
					|| (str == "Energy1"  && value["energy"] <= 20.0)
					|| (str == "Energy2" && value["energy"] > 20.0 && value["energy"] < 20.0)
					|| (str == "Energy3"  && value["energy"] >= 50.0)
					|| (str == "Carbonhydrate1"  && value["carbonhydrate"] <= 10.0)
					|| (str == "Carbonhydrate2" && value["carbonhydrate"] > 10.0 && value["carbonhydrate"] < 20.0)
					|| (str == "Carbonhydrate3"  && value["carbonhydrate"] >= 20.0)
					|| (str == "Fiber1"  && value["fiber"] <= 1.0)
					|| (str == "Fiber2" && value["fiber"] > 1.0 && value["fiber"] < 5.0)
					|| (str == "Fiber3"  && value["fiber"] >= 5.0)
					|| (str == "Sodium1"  && value["sodium"] < 1.0)
					|| (str == "Sodium2" && value["sodium"] >= 1.0 && value["sodium"] < 10.0)
					|| (str == "Sodium3"  && value["sodium"] >= 10.0)
					|| (str == "Potassium1"  && value["potassium"] < 100.0)
					|| (str == "Potassium2" && value["potassium"] >= 100.0 && value["potassium"] < 500.0)
					|| (str == "Potassium3"  && value["potassium"] >= 500.0)){
	        count++;
	    }
    }

    this.setState({
		page: 0,
		lastPage: Math.ceil(count/9),
      	filterStatus: str,
      	modelSpinnerStatus: "model-spinner-hidden"
    });
    
  }

  searchHandleClick = () => {
    const { items } = this.state;
    let res = this.state.tempKeyword.split(" ");
	let count = 0;

    for (const [index, value] of items.entries()) {
    	for (const word of res) {
    		if (value["name"].toUpperCase().includes(word.toUpperCase()) || value["servingUnit"].toUpperCase().includes(word.toUpperCase())) {
    			count++;
    			break;
    		}
    	}
    }

    this.setState({
		page: 0,
		lastPage: Math.ceil(count/9),
		searchKeyword: this.state.tempKeyword,
      	modelSpinnerStatus: "model-spinner-hidden"
    });
  }

  sortButtonHandleClick(e, str) {
    e.preventDefault();
    const { items } = this.state;
    let newItemList = items
    if (str == "NameAscending"){
        newItemList = newItemList.sort(function(a, b) {
        var nameA = a["name"].toUpperCase(); // ignore upper and lowercase
        var nameB = b["name"].toUpperCase(); // ignore upper and lowercase
        if (nameA < nameB) {
          return -1;
        }
        if (nameA > nameB) {
          return 1;
        }
        // names must be equal
        return 0;
      })
    } else if (str == "NameDescending") {
    	newItemList = newItemList.sort(function(a, b) {
        var nameA = a["name"].toUpperCase(); // ignore upper and lowercase
        var nameB = b["name"].toUpperCase(); // ignore upper and lowercase
        if (nameA < nameB) {
          return 1;
        }
        if (nameA > nameB) {
          return -1;
        }
        // names must be equal
        return 0;
      })
	} else if (str.includes("Ascending")){
		var substr = str.substring(0, str.length - 9);
        newItemList = newItemList.sort((a, b) => a[substr] - b[substr])
    } else if (str.includes("Descending")){
    	var substr = str.substring(0, str.length - 10);
        newItemList = newItemList.sort((a, b) => -a[substr] + b[substr])
    }
    
    // newItemList = newItemList.reverse()
    this.setState({
        page: 0,
        sortBy: str,
        items: newItemList
    });
  }


  render() {
    let { error, isLoaded, page, items, modelSpinnerStatus, filterStatus, lastPage, searchKeyword, sortBy } = this.state;
    let imgList = [];
    let titleList = [];
    let servingUnitList = [];

    //Keyword typed by the user
    console.log(searchKeyword);
    let res = searchKeyword.split(" ");
    if (error) {
      items = null;     
    } else if (!isLoaded) {
      return (<div className="spinner-about">
        <Spinner animation="grow" size="lg" />
        </div>);
    } else {
    	if (!searchKeyword == "") {
    		for (const [index, value] of items.entries()) {
    			for (const word of res) {
		    		if (value["name"].toUpperCase().includes(word.toUpperCase()) || value["servingUnit"].toUpperCase().includes(word.toUpperCase())) {
		    			imgList.push(value["image"]);
					    titleList.push(value["name"]);
					    servingUnitList.push(value["servingUnit"])
		    			break;
		    		}
		    	}
		    }
			page = 0;
			lastPage = Math.ceil(titleList.length/9);
    	} else {
			for (const [index, value] of items.entries()) {
				if (filterStatus == "" 	|| filterStatus == "Carbonhydrate0" || filterStatus == "Fiber0" 
						|| filterStatus == "Energy0" || filterStatus == "Sodium0" || filterStatus == "Potassium0" 
						|| (filterStatus == "AH" && value["name"].charAt(0) >= 'A' && value["name"].charAt(0) <= 'H')
						|| (filterStatus == "IP" && value["name"].charAt(0) >= 'I' && value["name"].charAt(0) <= 'P')
						|| (filterStatus == "QZ" && value["name"].charAt(0) >= 'Q' && value["name"].charAt(0) <= 'Z')
						|| (filterStatus == "Energy1"  && value["energy"] < 20.0)
						|| (filterStatus == "Energy2" && value["energy"] >= 20.0 && value["energy"] < 50.0)
						|| (filterStatus == "Energy3"  && value["energy"] >= 50.0)
						|| (filterStatus == "Carbonhydrate1"  && value["carbonhydrate"] < 10.0)
						|| (filterStatus == "Carbonhydrate2" && value["carbonhydrate"] >= 10.0 && value["carbonhydrate"] < 20.0)
						|| (filterStatus == "Carbonhydrate3"  && value["carbonhydrate"] >= 20.0)
						|| (filterStatus == "Fiber1"  && value["fiber"] < 1.0)
						|| (filterStatus == "Fiber2" && value["fiber"] >= 1.0 && value["fiber"] < 5.0)
						|| (filterStatus == "Fiber3"  && value["fiber"] >= 5.0)
						|| (filterStatus == "Sodium1"  && value["sodium"] < 1.0)
						|| (filterStatus == "Sodium2" && value["sodium"] >= 1.0 && value["sodium"] < 10.0)
						|| (filterStatus == "Sodium3"  && value["sodium"] >= 10.0)
						|| (filterStatus == "Potassium1"  && value["potassium"] < 100.0)
						|| (filterStatus == "Potassium2" && value["potassium"] >= 100.0 && value["potassium"] < 500.0)
						|| (filterStatus == "Potassium3"  && value["potassium"] >= 500.0)){
			    imgList.push(value["image"]);
			    titleList.push(value["name"]);
			    if (filterStatus.includes("Carbonhydrate") || sortBy.includes("carbonhydrate")) {
			    	servingUnitList.push(value["carbonhydrate"]);
			    } else if (filterStatus.includes("Fiber") || sortBy.includes("fiber")){
					servingUnitList.push(value["fiber"]);
				} else if (filterStatus.includes("Energy") || sortBy.includes("energy")){
					servingUnitList.push(value["energy"]);
				} else if (filterStatus.includes("Sodium") || sortBy.includes("sodium")){
					servingUnitList.push(value["sodium"]);
				} else if (filterStatus.includes("Potassium") || sortBy.includes("potassium")){
					servingUnitList.push(value["potassium"]);
				} else {
			    	servingUnitList.push(value["servingUnit"]);
			    }
			}
		}
	}

	let attribute1 = "servingUnit: ";
	if (filterStatus.includes("Carbonhydrate") || sortBy.includes("carbonhydrate")) {
		attribute1 = "Carbonhydrate: ";
	} else if (filterStatus.includes("Fiber") || sortBy.includes("fiber")) {
		attribute1 = "Fiber: ";
	} else if (filterStatus.includes("Energy") || sortBy.includes("energy")) {
		attribute1 = "Energy: ";
	} else if (filterStatus.includes("Sodium") || sortBy.includes("sodium")) {
		attribute1 = "Sodium: ";
	} else if (filterStatus.includes("Potassium") || sortBy.includes("potassium")) {
		attribute1 = "Potassium: ";
	} 
	if (searchKeyword.length != 0){
		attribute1 = "servingUnit: ";
	}
	let count = page * 9;
	let range = 9;
	if (page == lastPage - 1) {
	range = titleList.length - count;
	}
	let numInstance = titleList.length;
	if (numInstance == 0){
		range = 0;
	}

	let status = "";
	if (filterStatus != "") {
		status = "/Filter by " + filterStatus.substring(0, filterStatus.length - 1);
	}
	if (sortBy != "") {
		if (sortBy.includes("Ascending")) {
			status += " /Sorted by " +  sortBy.substring(0, sortBy.length - 9) + " in ascending order";
		} else if (sortBy.includes("Descending")) {
			status += " /Sorted by " +  sortBy.substring(0, sortBy.length - 10) + " in descending order";
		}
	}

	return(
		<>
		<div className="spinner-about">
		<Spinner className={modelSpinnerStatus} animation="grow" size="lg" />
		</div>
		<Header></Header>
		<br/>
		<h2 id="model-content" className="model-title">VEGETABLE PAGE({numInstance} Instances){status}</h2>
		<br/>

		<Form inline className="justify-content-md-center">
            <Form.Control 
            	type="text" 
            	placeholder="Search" 
            	className="mr-sm-2" 
            	value={this.state.value} onChange={this.handleChange}
			/>
            <Button variant="outline-info" onClick={this.searchHandleClick}>Search</Button>
    	</Form>

      	<ButtonToolbar aria-label="Toolbar with button groups" className="justify-content-md-center">
      	<h2>Filter by</h2>
      		<Dropdown>
				<Dropdown.Toggle variant="success" id="dropdown-basic">
				Name
				</Dropdown.Toggle>

				<Dropdown.Menu>
					<Dropdown.Item>
						<Link onClick={e => this.buttonHandleClick(e, "AH")} href="#">
					      	{"A-H"}
					    </Link>
					</Dropdown.Item>
					<Dropdown.Item>
						<Link onClick={e => this.buttonHandleClick(e, "IP")} href="#">
					      	{"I-P"}
					    </Link>
					</Dropdown.Item>
					<Dropdown.Item>
						<Link onClick={e => this.buttonHandleClick(e, "QZ")} href="#">
					      	{"Q-Z"}
					    </Link>
					</Dropdown.Item>
					<Dropdown.Item>
						<Link onClick={e => this.buttonHandleClick(e, "")} href="#">
					      	{"All"}
					    </Link>
					</Dropdown.Item>
				</Dropdown.Menu>
			</Dropdown>

			<Dropdown>
				<Dropdown.Toggle variant="primary" id="dropdown-basic">
					Energy
				</Dropdown.Toggle>

				<Dropdown.Menu>
					<Dropdown.Item>
						<Link onClick={e => this.buttonHandleClick(e, "Energy1")} href="#">
					      	{"0-20"}
					    </Link>
					</Dropdown.Item>
					<Dropdown.Item>
						<Link onClick={e => this.buttonHandleClick(e, "Energy2")} href="#">
					      	{"20-50"}
					    </Link>
					</Dropdown.Item>
					<Dropdown.Item>
						<Link onClick={e => this.buttonHandleClick(e, "Energy3")} href="#">
					      	{">50"}
					    </Link>
					</Dropdown.Item>
					<Dropdown.Item>
						<Link onClick={e => this.buttonHandleClick(e, "Energy0")} href="#">
					      	{"All"}
					    </Link>
					</Dropdown.Item>
				</Dropdown.Menu>
			</Dropdown>

			<Dropdown>
				<Dropdown.Toggle variant="success" id="dropdown-basic">
					Carbonhydrate
				</Dropdown.Toggle>

				<Dropdown.Menu>
					<Dropdown.Item>
						<Link onClick={e => this.buttonHandleClick(e, "Carbonhydrate1")} href="#">
					      	{"0-10"}
					    </Link>
					</Dropdown.Item>
					<Dropdown.Item>
						<Link onClick={e => this.buttonHandleClick(e, "Carbonhydrate2")} href="#">
					      	{"10-20"}
					    </Link>
					</Dropdown.Item>
					<Dropdown.Item>
						<Link onClick={e => this.buttonHandleClick(e, "Carbonhydrate3")} href="#">
					      	{">20"}
					    </Link>
					</Dropdown.Item>
					<Dropdown.Item>
						<Link onClick={e => this.buttonHandleClick(e, "Carbonhydrate0")} href="#">
					      	{"All"}
					    </Link>
					</Dropdown.Item>
				</Dropdown.Menu>
			</Dropdown>

			<Dropdown>
				<Dropdown.Toggle variant="primary" id="dropdown-basic">
					Fiber
				</Dropdown.Toggle>

				<Dropdown.Menu>
					<Dropdown.Item>
						<Link onClick={e => this.buttonHandleClick(e, "Fiber1")} href="#">
					      	{"0-1"}
					    </Link>
					</Dropdown.Item>
					<Dropdown.Item>
						<Link onClick={e => this.buttonHandleClick(e, "Fiber2")} href="#">
					      	{"1-5"}
					    </Link>
					</Dropdown.Item>
					<Dropdown.Item>
						<Link onClick={e => this.buttonHandleClick(e, "Fiber3")} href="#">
					      	{">5"}
					    </Link>
					</Dropdown.Item>
					<Dropdown.Item>
						<Link onClick={e => this.buttonHandleClick(e, "Fiber0")} href="#">
					      	{"All"}
					    </Link>
					</Dropdown.Item>
				</Dropdown.Menu>
			</Dropdown>

			<Dropdown>
				<Dropdown.Toggle variant="success" id="dropdown-basic">
					Sodium
				</Dropdown.Toggle>

				<Dropdown.Menu>
					<Dropdown.Item>
						<Link onClick={e => this.buttonHandleClick(e, "Sodium1")} href="#">
					      	{"0-1.0"}
					    </Link>
					</Dropdown.Item>
					<Dropdown.Item>
						<Link onClick={e => this.buttonHandleClick(e, "Sodium2")} href="#">
					      	{"1.0-10.0"}
					    </Link>
					</Dropdown.Item>
					<Dropdown.Item>
						<Link onClick={e => this.buttonHandleClick(e, "Sodium3")} href="#">
					      	{">10.0"}
					    </Link>
					</Dropdown.Item>
					<Dropdown.Item>
						<Link onClick={e => this.buttonHandleClick(e, "Sodium0")} href="#">
					      	{"All"}
					    </Link>
					</Dropdown.Item>
				</Dropdown.Menu>
			</Dropdown>

			<Dropdown>
				<Dropdown.Toggle variant="primary" id="dropdown-basic">
					Potassium
				</Dropdown.Toggle>

				<Dropdown.Menu>
					<Dropdown.Item>
						<Link onClick={e => this.buttonHandleClick(e, "Potassium1")} href="#">
					      	{"0-100"}
					    </Link>
					</Dropdown.Item>
					<Dropdown.Item>
						<Link onClick={e => this.buttonHandleClick(e, "Potassium2")} href="#">
					      	{"100-500"}
					    </Link>
					</Dropdown.Item>
					<Dropdown.Item>
						<Link onClick={e => this.buttonHandleClick(e, "Potassium3")} href="#">
					      	{">500"}
					    </Link>
					</Dropdown.Item>
					<Dropdown.Item>
						<Link onClick={e => this.buttonHandleClick(e, "Potassium0")} href="#">
					      	{"All"}
					    </Link>
					</Dropdown.Item>
				</Dropdown.Menu>
			</Dropdown>

      	</ButtonToolbar>

      	<ButtonToolbar aria-label="Toolbar with button groups" className="justify-content-md-center">
      		<h2>Sorted by</h2>

      		<Dropdown>
				<Dropdown.Toggle variant="primary" id="dropdown-basic">
					Name
				</Dropdown.Toggle>

				<Dropdown.Menu>
					<Dropdown.Item>
						<Link onClick={e => this.sortButtonHandleClick(e, "NameAscending")} href="#">
					      	{"Ascending"}
					    </Link>
					</Dropdown.Item>
					<Dropdown.Item>
						<Link onClick={e => this.sortButtonHandleClick(e, "NameDescending")} href="#">
					      	{"Descending"}
					    </Link>
					</Dropdown.Item>
				</Dropdown.Menu>
			</Dropdown>

			<Dropdown>
				<Dropdown.Toggle variant="success" id="dropdown-basic">
					Energy
				</Dropdown.Toggle>

				<Dropdown.Menu>
					<Dropdown.Item>
						<Link onClick={e => this.sortButtonHandleClick(e, "energyAscending")} href="#">
					      	{"Ascending"}
					    </Link>
					</Dropdown.Item>
					<Dropdown.Item>
						<Link onClick={e => this.sortButtonHandleClick(e, "energyDescending")} href="#">
					      	{"Descending"}
					    </Link>
					</Dropdown.Item>
				</Dropdown.Menu>
			</Dropdown>

			<Dropdown>
				<Dropdown.Toggle variant="primary" id="dropdown-basic">
					Carbonhydrate
				</Dropdown.Toggle>

				<Dropdown.Menu>
					<Dropdown.Item>
						<Link onClick={e => this.sortButtonHandleClick(e, "carbonhydrateAscending")} href="#">
					      	{"Ascending"}
					    </Link>
					</Dropdown.Item>
					<Dropdown.Item>
						<Link onClick={e => this.sortButtonHandleClick(e, "carbonhydrateDescending")} href="#">
					      	{"Descending"}
					    </Link>
					</Dropdown.Item>
				</Dropdown.Menu>
			</Dropdown>

			<Dropdown>
				<Dropdown.Toggle variant="success" id="dropdown-basic">
					Fiber
				</Dropdown.Toggle>

				<Dropdown.Menu>
					<Dropdown.Item>
						<Link onClick={e => this.sortButtonHandleClick(e, "fiberAscending")} href="#">
					      	{"Ascending"}
					    </Link>
					</Dropdown.Item>
					<Dropdown.Item>
						<Link onClick={e => this.sortButtonHandleClick(e, "fiberDescending")} href="#">
					      	{"Descending"}
					    </Link>
					</Dropdown.Item>
				</Dropdown.Menu>
			</Dropdown>

			<Dropdown>
				<Dropdown.Toggle variant="primary" id="dropdown-basic">
					Sodium
				</Dropdown.Toggle>

				<Dropdown.Menu>
					<Dropdown.Item>
						<Link onClick={e => this.sortButtonHandleClick(e, "sodiumAscending")} href="#">
					      	{"Ascending"}
					    </Link>
					</Dropdown.Item>
					<Dropdown.Item>
						<Link onClick={e => this.sortButtonHandleClick(e, "sodiumDescending")} href="#">
					      	{"Descending"}
					    </Link>
					</Dropdown.Item>
				</Dropdown.Menu>
			</Dropdown>

			<Dropdown>
				<Dropdown.Toggle variant="success" id="dropdown-basic">
					Potassium
				</Dropdown.Toggle>

				<Dropdown.Menu>
					<Dropdown.Item>
						<Link onClick={e => this.sortButtonHandleClick(e, "potassiumAscending")} href="#">
					      	{"Ascending"}
					    </Link>
					</Dropdown.Item>
					<Dropdown.Item>
						<Link onClick={e => this.sortButtonHandleClick(e, "potassiumDescending")} href="#">
					      	{"Descending"}
					    </Link>
					</Dropdown.Item>
				</Dropdown.Menu>
			</Dropdown>
      	</ButtonToolbar>

      	<br/><br/>

		<Container>
		<Grid
                                container
                                spacing={9}
                                direction="row"
                                alignItems="flex-start"
                                justify="flex-start"
                                style={{ minHeight: '100vh' }}
                            >
		  	{[...Array(range)].map((page1, i) =>
		    <Grid item xs={12} sm={6} md={4} key={"Veg"+i}>
		      <Card className="model-card">
		          <Link to={"/vegetables/"+titleList[count + i]}>
		            <Card.Img className="model-card-img" variant="top" src={imgList[count + i]} height="250rem"/>
		          </Link>
		          <Card.Body>
		            <Link to={"/vegetables/"+titleList[count + i]}>
		              <Card.Title className="model-instance-title">
		              <Highlighter
						    highlightClassName="YourHighlightClass"
						    searchWords={res}
						    autoEscape={true}
						    textToHighlight={titleList[count + i]}
					  />
		              </Card.Title>
		            </Link>
		            <Card.Text>{attribute1}
		            <Highlighter
					    highlightClassName="YourHighlightClass"
					    searchWords={res}
					    autoEscape={true}
					    textToHighlight={servingUnitList[count + i].toString()}
				  	/>
		            </Card.Text>
		          </Card.Body>
		      </Card>
		    </Grid>
		  )}
		</Grid>
		</Container>

      <br/><br/>

      <Pagination className="justify-content-md-center">
            <Pagination.Item disabled={page <=  0} onClick={e => this.handleClick(e, page - 1)}>
            	<b>Previous</b>
            </Pagination.Item>

            {[...Array(lastPage)].map((page1, i) => 
              	<Pagination.Item active={i === page} key={i} onClick={e => this.handleClick(e, i)}>
                  	{i + 1}
              	</Pagination.Item>
            )}

            <Pagination.Next disabled={page >= lastPage - 1} onClick={e => this.handleClick(e, page + 1)}>
                <b>Next</b>
            </Pagination.Next>
            
      </Pagination>

      <Footer></Footer>
      </>
      );
      }
    }
  }

export default Vegetables;
