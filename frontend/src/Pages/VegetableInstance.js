import React, {Component} from 'react';
import {Nav, Navbar, Form, Button, Container} from 'react-bootstrap';
import Card from "react-bootstrap/Card";
import CardDeck from 'react-bootstrap/CardDeck'
import Header from '../Components/Header'
import Footer from '../Components/Footer'
import Spinner from 'react-bootstrap/Spinner';
import { Redirect } from 'react-router-dom';

class VegetableInstance extends Component {
    constructor(props) {
          super(props);
          this.state = {
            error: null,
            isLoaded: false,
            items: []
          };
      }

    componentDidMount() {
    fetch("https://www.foodninja.club/api/vegetable?name="+encodeURIComponent(this.props.match.params.name), {method: 'get'})
      .then(res => res.json())
      .then(
          (result) => {
              this.setState({
                  isLoaded: true,
                  items: result
              });
          },
          (error) => {
              this.setState({
                  commits:{
                      isLoaded: true,
                      error
                  }
              });
          }
      );
    }

    render() {
        let { isLoaded, error, items } = this.state;
        console.log(this.props.match.params.name)
        if (error) {
          items = null;     
        } else if (!isLoaded) {
          return (<div className="spinner-about">
            <Spinner animation="grow" size="lg" />
            </div>);
        } else if(items.length == 0) {
            return(<Redirect to="/404" />);
        } else {
            return(
                <>
                    <Header></Header>
                    <br/>
                        <h2 className="instance-title">VEGETABLE PAGE</h2>
                    <br/>
                    <Container className="instance-container">
                        <div className="instance-div">
                            <CardDeck>
                                <Card>
                                    <Card.Img src={items[0]["image"]} className="instance-card-img"/>
                                </Card>
                                <Card>
                                    <Card.Body>
                                        <Card.Title>{items[0]["name"]}</Card.Title>
                                        <Card.Text><a href="/nutrition/energy">Energy</a>: {items[0]["energy"]}</Card.Text>
                                        <Card.Text><a href="/nutrition/cholesterol">Cholesterol</a>: {items[0]["cholesterol"]} </Card.Text>
                                        <Card.Text><a href="/nutrition/fiber">Fiber</a>: {items[0]["fiber"]} </Card.Text>
                                        <Card.Text><a href="/nutrition/potassium">Potassium</a>: {items[0]["potassium"]} </Card.Text>
                                        <Card.Text><a href="/nutrition/protein">Protein</a>: {items[0]["protein"]} </Card.Text>
                                        <Card.Text><a href="/nutrition/sodium">Sodium</a>: {items[0]["sodium"]} </Card.Text>
                                        <Card.Text><a href="/nutrition/Carbonhydrate">Carbonhydrate</a>: {items[0]["carbonhydrate"]} </Card.Text>
                                        <Card.Text><a href="/nutrition/fat">TotalFat</a>: {items[0]["totalFat"]} </Card.Text>
                                    </Card.Body>
                                </Card>
                            </CardDeck>
                        </div>
                    </Container>
                    <Footer></Footer>
                 </>
                );
            }
        }
    }

export default VegetableInstance;