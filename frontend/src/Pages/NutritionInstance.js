import React, {Component} from 'react';
import {Nav, Navbar, Form, Button, Container} from 'react-bootstrap';
import Card from "react-bootstrap/Card";
import CardDeck from 'react-bootstrap/CardDeck'
import Header from '../Components/Header'
import Footer from '../Components/Footer'
import Spinner from 'react-bootstrap/Spinner';
import { Redirect } from 'react-router-dom';

class NutritionInstance extends Component {
    constructor(props) {
          super(props);
          this.state = {
            error: null,
            isLoaded: false,
            items: []
          };
      }

    componentDidMount() {
    fetch("https://www.foodninja.club/api/nutrition?name="+encodeURIComponent(this.props.match.params.name), {method: 'get'})
      .then(res => res.json())
      .then(
          (result) => {
              this.setState({
                  isLoaded: true,
                  items: result
              });
          },
          (error) => {
              this.setState({
                  commits:{
                      isLoaded: true,
                      error
                  }
              });
          }
      );
    }

    render() {
        let { isLoaded, error, items } = this.state;
        console.log(this.props.match.params.name)
        if (error) {
          items = null;     
        } else if (!isLoaded) {
          return (<div className="spinner-about">
            <Spinner animation="grow" size="lg" />
            </div>);
        } else if(items.length == 0) {
            return(<Redirect to="/404" />);
        } else {
            return(
                <>
                    <Header></Header>
                    <br/>
                        <h2 className="instance-title">NUTRITION PAGE</h2>
                    <br/>
                    <Container className="instance-container">
                        <div className="instance-div">
                            <CardDeck>
                                <Card>
                                    <Card.Img src={items[0]["image"]} className="instance-card-img"/>
                                </Card>
                                <Card>
                                    <Card.Body>
                                        <Card.Title>{items[0]["name"]}</Card.Title>
                                        <Card.Text>Description: {items[0]["description"]}</Card.Text>
                                        <Card.Text>Formula: {items[0]["formula"]}</Card.Text>
                                        <Card.Text>Example: {items[0]["example"]} </Card.Text>
                                        <Card.Text>Carbon: {items[0]["carbon"]} </Card.Text>
                                        <Card.Text>Hydrogen: {items[0]["hydrogen"]} </Card.Text>
                                        <Card.Text>Oxygen: {items[0]["oxygen"]} </Card.Text>
                                    </Card.Body>
                                </Card>
                            </CardDeck>
                        </div>
                    </Container>
                    <Footer></Footer>
                 </>
                );
            }
        }
    }

export default NutritionInstance;


