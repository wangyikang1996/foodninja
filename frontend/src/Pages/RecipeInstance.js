import React, {Component} from 'react';
import {Nav, Navbar, Form, Button, Container} from 'react-bootstrap';
import Card from "react-bootstrap/Card";
import CardDeck from 'react-bootstrap/CardDeck'
import Header from '../Components/Header'
import Footer from '../Components/Footer'
import Pic from '../IMG/recipe1.jpg';
import Spinner from 'react-bootstrap/Spinner';
import { Redirect } from 'react-router-dom';
import {Link} from 'react-router-dom';


class RecipeInstance extends Component {
    constructor(props) {
          super(props);
          this.state = {
            error: null,
            isLoaded: false,
            fruitItems: [],
            recipeItems: [],
            vegetableItems: [],
            fruitErr: null,
            vegetableErr: null,
            fruitIsLoaded: false,
            vegetableIsLoaded: false
          };
      }

    componentDidMount() {
        fetch("https://www.foodninja.club/api/recipe?title="+encodeURIComponent(this.props.match.params.name), {method: 'get'})
          .then(res => res.json())
          .then(
              (result) => {
                  this.setState({
                      isLoaded: true,
                      recipeItems: result
                  });
              },
              (error) => {
                  this.setState({
                      isLoaded: true,
                      error: error
                  });
              }
          );

          fetch("https://www.foodninja.club/api/fruit/all", {method: 'get'})
          .then(res => res.json())
          .then(
              (result) => {
                  this.setState({
                      fruitItems: result,
                      fruitIsLoaded: true
                  });
              },
              (error) => {
                  this.setState({
                      isLoaded: true,
                      fruitErr: error
                  });
              }
          );

          fetch("https://www.foodninja.club/api/vegetable/all", {method: 'get'})
          .then(res => res.json())
          .then(
              (result) => {
                  this.setState({
                      vegetableItems: result,
                      vegetableIsLoaded: true
                  });
              },
              (error) => {
                  this.setState({
                      isLoaded: true,
                      vegetableErr: error
                  });
              }
          );
    }

    generateLinking(){
        let usedIngredientList = [];
        let returnComponenets = [];
        if(!this.state.vegetableIsLoaded || !this.state.fruitIsLoaded){
            return(this.state.recipeItems[0]["usedIngredients"]);
        }
        if(this.state.fruitErr != null || this.state.vegetableErr != null){
            return(this.state.recipeItems[0]["usedIngredients"]);
        }
        if(this.state.vegetableItems.length != 0 && this.state.fruitItems.length != 0){
            usedIngredientList = this.state.recipeItems[0]["usedIngredients"].split(",");
            for (const [ingIndex, ingValue] of usedIngredientList.entries()) {
                for (const [vegIndex, vegValue] of this.state.vegetableItems.entries()){
                    if(vegValue["name"].toLowerCase() == ingValue){
                        if(ingIndex == 0){
                            returnComponenets.push(
                            <a key={ingValue} href={"/vegetables/"+ingValue}>{ingValue}</a>
                            )
                        }
                        else{
                            returnComponenets.push(
                            <a key={ingValue} href={"/vegetables/"+ingValue}>,{ingValue}</a>
                            )
                        }
                        
                    }
                }

                for (const [fruitIndex, fruitValue] of this.state.fruitItems.entries()){
                    if(fruitValue["name"].toLowerCase() == ingValue){
                        if(ingIndex == 0){
                            returnComponenets.push(
                            <a key={ingValue} href={"/fruits/"+ingValue}>{ingValue}</a>
                            )
                        }
                        else{
                            returnComponenets.push(
                            <a key={ingValue} href={"/fruits/"+ingValue}>,{ingValue}</a>
                            )
                        }
                    }
                }
            }
            if(returnComponenets.length == 0){
               returnComponenets.push(this.state.recipeItems[0]["usedIngredients"]); 
            }
            return(returnComponenets);
        }
        return(this.state.recipeItems[0]["usedIngredients"]);
    }

    render() {
        let { isLoaded, error, recipeItems, fruitItems, vegetableItems, fruitIsLoaded, vegetableIsLoaded} = this.state;
        let usedIngredients = "";
        if (error) {
          recipeItems = null;     
        } else if (!isLoaded) {
          return (<div className="spinner-about">
            <Spinner animation="grow" size="lg" />
            </div>);
        } else if(recipeItems.length == 0) {
            return(<Redirect to="/404" />);
        } else {
            console.log(recipeItems);
            if(recipeItems[0]["usedIngredients"]){
                usedIngredients = recipeItems[0]["usedIngredients"] + ","
            }
            return(
                <>
    		        <Header></Header>
                    <br/>
                        <h2 className="instance-title">RECIPE PAGE</h2>
                    <br/>
                    <Container className="instance-container">
                        <div className="instance-div">
                            <CardDeck>
                                <Card>
                                    <Card.Img src={recipeItems[0]["image"]} className="instance-card-img"/>
                                </Card>
                                <Card>
                                    <Card.Body>
                                        <Card.Title>{recipeItems[0]["title"]}</Card.Title>
                                        <Card.Text>IngredientCount: {recipeItems[0]["usedIngredientCount"]} </Card.Text>
                                        <Card.Text>Ingredients: {this.generateLinking()}</Card.Text>
                                        <Card.Text>MissedIngredientCount: {recipeItems[0]["missedIngredientCount"]} </Card.Text>
                                        <Card.Text>MissedIngredient: {recipeItems[0]["missedIngredients"]} </Card.Text>
                                        <Card.Text>Aisle: {recipeItems[0]["aisle"]} </Card.Text>
                                        <Card.Text>Likes: {recipeItems[0]["likes"]} </Card.Text>
                                    </Card.Body>
                                </Card>
                            </CardDeck>
                        </div>
                    </Container>
    		        <Footer></Footer>
    		     </>
                );
            }
        }
    }

export default RecipeInstance;