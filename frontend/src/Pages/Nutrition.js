import React, {Component} from 'react';
import {Nav, Navbar, Form, Button, Card, Row, Col} from 'react-bootstrap';
import Dropdown from 'react-bootstrap/Dropdown';
import {Link} from 'react-router-dom';
import CardDeck from 'react-bootstrap/CardDeck';
import Container from 'react-bootstrap/Container';
import Header from '../Components/Header';
import Footer from '../Components/Footer';
import Spinner from 'react-bootstrap/Spinner';
import Pagination from 'react-bootstrap/Pagination';
import PageItem from 'react-bootstrap/PageItem';
import ButtonToolbar from 'react-bootstrap/ButtonToolbar';
import Highlighter from "react-highlight-words";
import Grid from "@material-ui/core/Grid";

class Nutrition extends Component {
  constructor(props) {
          super(props);
          this.state = {
            error: null,
            isLoaded: false,
            page: 0,
            lastPage: 1,
            items: [],
            modelSpinnerStatus: "model-spinner-hidden",
            filterStatus: "",
            sortBy: "",
            searchKeyword: "", 
            tempKeyword: ""
          };
          this.handleChange = this.handleChange.bind(this);
          this.searchHandleClick = this.searchHandleClick.bind(this);
  }

  componentDidMount() {
    fetch("https://www.foodninja.club/api/nutrition/all/", {method: 'get'})
      .then(res => res.json())
      .then(
          (result) => {
              this.setState({
                  isLoaded: true,
                  page: 0,
                  items: result,
                  lastPage: Math.ceil(result.length/9),
                  modelSpinnerStatus: "model-spinner-hidden",
              });
          },
          (error) => {
              this.setState({
                  commits:{
                      isLoaded: true,
                      page: 0,
                      lastPage: 1,
                      modelSpinnerStatus: "model-spinner-hidden",
                      error
                  }
              });
          }
      );
    }

  handleClick(e, index) {
    
    e.preventDefault();
    let count = 0;

    this.setState({
      page: index
    });
  }

  handleChange(event) {
    this.setState({tempKeyword: event.target.value});
  }

  buttonHandleClick(e, str) {
    
    e.preventDefault();

    let { items } = this.state;
    let count = 0;
    for (const [index, value] of items.entries()) {
      	if (str == "" || str == "Carbon0" || str == "Hydrogen0" || str == "Oxygen0" 
      				|| (str == "AH" && value["name"].charAt(0) >= 'A' && value["name"].charAt(0) <= 'H')
					|| (str == "IP" && value["name"].charAt(0) >= 'I' && value["name"].charAt(0) <= 'P')
					|| (str == "QZ" && value["name"].charAt(0) >= 'Q' && value["name"].charAt(0) <= 'Z')
					|| (str == "Carbon1"  && value["carbon"] == 0)
					|| (str == "Carbon2" && value["carbon"] > 0 && value["carbon"] < 10)
					|| (str == "Carbon3"  && value["carbon"] >= 10.0)
					|| (str == "Hydrogen1"  && value["hydrogen"] == 0)
					|| (str == "Hydrogen2" && value["hydrogen"] > 0 && value["hydrogen"] < 10)
					|| (str == "Hydrogen3"  && value["hydrogen"] >= 10)
					|| (str == "Oxygen1"  && value["oxygen"] == 0)
					|| (str == "Oxygen2" && value["oxygen"] > 0 && value["oxygen"] < 5)
					|| (str == "Oxygen3"  && value["oxygen"] >= 5)){
	        count++;
	    }
    }

    this.setState({
		page: 0,
		lastPage: Math.ceil(count/9),
      	filterStatus: str,
      	modelSpinnerStatus: "model-spinner-hidden"
    });
    
  }

  searchHandleClick = () => {
  	const { items } = this.state;
    let res = this.state.tempKeyword.split(" ");
	let count = 0;

    for (const [index, value] of items.entries()) {
    	for (const word of res) {
    		if (value["name"].toUpperCase().includes(word.toUpperCase()) || value["formula"].toUpperCase().includes(word.toUpperCase())) {
    			count++;
    			break;
    		}
    	}
    }

    this.setState({
		page: 0,
		lastPage: Math.ceil(count/9),
		searchKeyword: this.state.tempKeyword,
      	modelSpinnerStatus: "model-spinner-hidden"
    });
  }

  sortButtonHandleClick(e, str) {
    e.preventDefault();
    const { items } = this.state;
    let newItemList = items
    if (str == "NameAscending"){
        newItemList = newItemList.sort(function(a, b) {
        var nameA = a["name"].toUpperCase(); // ignore upper and lowercase
        var nameB = b["name"].toUpperCase(); // ignore upper and lowercase
        if (nameA < nameB) {
          return -1;
        }
        if (nameA > nameB) {
          return 1;
        }
        // names must be equal
        return 0;
      })
    } else if (str == "NameDescending") {
    	newItemList = newItemList.sort(function(a, b) {
        var nameA = a["name"].toUpperCase(); // ignore upper and lowercase
        var nameB = b["name"].toUpperCase(); // ignore upper and lowercase
        if (nameA < nameB) {
          return 1;
        }
        if (nameA > nameB) {
          return -1;
        }
        // names must be equal
        return 0;
      })
	} else if (str.includes("Ascending")){
		var substr = str.substring(0, str.length - 9);
        newItemList = newItemList.sort((a, b) => a[substr] - b[substr])
    } else if (str.includes("Descending")){
    	var substr = str.substring(0, str.length - 10);
        newItemList = newItemList.sort((a, b) => -a[substr] + b[substr])
    }
    
    // newItemList = newItemList.reverse()
    this.setState({
        page: 0,
        sortBy: str,
        items: newItemList
    });
  }


  render() {
    let { error, isLoaded, page, items, modelSpinnerStatus, filterStatus, lastPage, searchKeyword, sortBy } = this.state;
    let imgList = [];
    let titleList = [];
    let formulaList = [];

    //Keyword typed by the user
    console.log(searchKeyword);
    let res = searchKeyword.split(" ");
    if (error) {
      items = null;     
    } else if (!isLoaded) {
      return (<div className="spinner-about">
        <Spinner animation="grow" size="lg" />
        </div>);
    } else {
    	if (!searchKeyword == "") {
    		for (const [index, value] of items.entries()) {
    			for (const word of res) {
		    		if (value["name"].toUpperCase().includes(word.toUpperCase()) || value["formula"].toUpperCase().includes(word.toUpperCase())) {
		    			imgList.push(value["image"]);
					    titleList.push(value["name"]);
					    formulaList.push(value["formula"])
		    			break;
		    		}
		    	}
		    }
			page = 0;
			lastPage = Math.ceil(titleList.length/9);
    	} else {
			for (const [index, value] of items.entries()) {
				if (filterStatus == "" 	|| filterStatus == "Carbon0" || filterStatus == "Hydrogen0" || filterStatus == "Oxygen0" 
						|| (filterStatus == "Carbon1"  && value["carbon"] == 0)
						|| (filterStatus == "Carbon2" && value["carbon"] > 0 && value["carbon"] < 10)
						|| (filterStatus == "Carbon3"  && value["carbon"] >= 10.0)
						|| (filterStatus == "Hydrogen1"  && value["hydrogen"] == 0)
						|| (filterStatus == "Hydrogen2" && value["hydrogen"] > 0 && value["hydrogen"] < 10)
						|| (filterStatus == "Hydrogen3"  && value["hydrogen"] >= 10)
						|| (filterStatus == "Oxygen1"  && value["oxygen"] == 0)
						|| (filterStatus == "Oxygen2" && value["oxygen"] > 0 && value["oxygen"] < 5)
						|| (filterStatus == "Oxygen3"  && value["oxygen"] >= 5)){
			    imgList.push(value["image"]);
			    titleList.push(value["name"]);
			    if (filterStatus.includes("Carbon") || sortBy.includes("carbon")) {
			    	formulaList.push(value["carbon"]);
			    } else if (filterStatus.includes("Hydrogen") || sortBy.includes("hydrogen")){
					formulaList.push(value["hydrogen"]);
				} else if (filterStatus.includes("Oxygen") || sortBy.includes("oxygen")){
					formulaList.push(value["oxygen"]);
				} else {
			    	formulaList.push(value["formula"]);
			    }
			}
		}
	}

	let attribute1 = "Formula: ";
	if (filterStatus.includes("Carbon") || sortBy.includes("carbon")) {
		attribute1 = "Carbon: ";
	} else if (filterStatus.includes("Hydrogen") || sortBy.includes("hydrogen")) {
		attribute1 = "Hydrogen: ";
	} else if (filterStatus.includes("Oxygen") || sortBy.includes("oxygen")) {
		attribute1 = "Oxygen: ";
	} 
	if (searchKeyword.length != 0){
		attribute1 = "Formula: ";
	}
	let count = page * 9;
	let range = 9;
	if (page == lastPage - 1) {
	range = titleList.length - count;
	}
	let numInstance = titleList.length;
	if (numInstance == 0){
		range = 0;
	}

	let status = "";
	if (filterStatus != "") {
		status = "/Filter by " + filterStatus.substring(0, filterStatus.length - 1);
	}
	if (sortBy != "") {
		if (sortBy.includes("Ascending")) {
			status += " /Sorted by " +  sortBy.substring(0, sortBy.length - 9) + " in ascending order";
		} else if (sortBy.includes("Descending")) {
			status += " /Sorted by " +  sortBy.substring(0, sortBy.length - 10) + " in descending order";
		}
	}

	return(
		<>
		<div className="spinner-about">
		<Spinner className={modelSpinnerStatus} animation="grow" size="lg" />
		</div>
		<Header></Header>
		<br/>
		<h2 id="model-content" className="model-title">NUTRITION PAGE({numInstance} Instances){status}</h2>
		<br/>

		<Form inline className="justify-content-md-center">
            <Form.Control 
            	type="text" 
            	placeholder="Search" 
            	className="mr-sm-2" 
            	value={this.state.value} onChange={this.handleChange}
			/>
            <Button variant="outline-info" onClick={this.searchHandleClick}>Search</Button>
    	</Form>

      	<ButtonToolbar aria-label="Toolbar with button groups" className="justify-content-md-center">
      	<h2>Filter by</h2>
			<Dropdown>
				<Dropdown.Toggle variant="success" id="dropdown-basic">
					Carbon
				</Dropdown.Toggle>

				<Dropdown.Menu>
					<Dropdown.Item>
						<Link onClick={e => this.buttonHandleClick(e, "Carbon1")} href="#">
					      	{"0"}
					    </Link>
					</Dropdown.Item>
					<Dropdown.Item>
						<Link onClick={e => this.buttonHandleClick(e, "Carbon2")} href="#">
					      	{"0-10"}
					    </Link>
					</Dropdown.Item>
					<Dropdown.Item>
						<Link onClick={e => this.buttonHandleClick(e, "Carbon3")} href="#">
					      	{">10"}
					    </Link>
					</Dropdown.Item>
					<Dropdown.Item>
						<Link onClick={e => this.buttonHandleClick(e, "Carbon0")} href="#">
					      	{"All"}
					    </Link>
					</Dropdown.Item>
				</Dropdown.Menu>
			</Dropdown>

			<Dropdown>
				<Dropdown.Toggle variant="primary" id="dropdown-basic">
					Hydrogen
				</Dropdown.Toggle>

				<Dropdown.Menu>
					<Dropdown.Item>
						<Link onClick={e => this.buttonHandleClick(e, "Hydrogen1")} href="#">
					      	{"0"}
					    </Link>
					</Dropdown.Item>
					<Dropdown.Item>
						<Link onClick={e => this.buttonHandleClick(e, "Hydrogen2")} href="#">
					      	{"0-10"}
					    </Link>
					</Dropdown.Item>
					<Dropdown.Item>
						<Link onClick={e => this.buttonHandleClick(e, "Hydrogen3")} href="#">
					      	{">10"}
					    </Link>
					</Dropdown.Item>
					<Dropdown.Item>
						<Link onClick={e => this.buttonHandleClick(e, "Hydrogen0")} href="#">
					      	{"All"}
					    </Link>
					</Dropdown.Item>
				</Dropdown.Menu>
			</Dropdown>

			<Dropdown>
				<Dropdown.Toggle variant="success" id="dropdown-basic">
					Oxygen
				</Dropdown.Toggle>

				<Dropdown.Menu>
					<Dropdown.Item>
						<Link onClick={e => this.buttonHandleClick(e, "Oxygen1")} href="#">
					      	{"0-0.2"}
					    </Link>
					</Dropdown.Item>
					<Dropdown.Item>
						<Link onClick={e => this.buttonHandleClick(e, "Oxygen2")} href="#">
					      	{"0.2-0.6"}
					    </Link>
					</Dropdown.Item>
					<Dropdown.Item>
						<Link onClick={e => this.buttonHandleClick(e, "Oxygen3")} href="#">
					      	{">0.6"}
					    </Link>
					</Dropdown.Item>
					<Dropdown.Item>
						<Link onClick={e => this.buttonHandleClick(e, "Oxygen0")} href="#">
					      	{"All"}
					    </Link>
					</Dropdown.Item>
				</Dropdown.Menu>
			</Dropdown>

      	</ButtonToolbar>

      	<ButtonToolbar aria-label="Toolbar with button groups" className="justify-content-md-center">
      		<h2>Sorted by</h2>

      		<Dropdown>
				<Dropdown.Toggle variant="primary" id="dropdown-basic">
					Name
				</Dropdown.Toggle>

				<Dropdown.Menu>
					<Dropdown.Item>
						<Link onClick={e => this.sortButtonHandleClick(e, "NameAscending")} href="#">
					      	{"Ascending"}
					    </Link>
					</Dropdown.Item>
					<Dropdown.Item>
						<Link onClick={e => this.sortButtonHandleClick(e, "NameDescending")} href="#">
					      	{"Descending"}
					    </Link>
					</Dropdown.Item>
				</Dropdown.Menu>
			</Dropdown>

			<Dropdown>
				<Dropdown.Toggle variant="success" id="dropdown-basic">
					Carbon
				</Dropdown.Toggle>

				<Dropdown.Menu>
					<Dropdown.Item>
						<Link onClick={e => this.sortButtonHandleClick(e, "carbonAscending")} href="#">
					      	{"Ascending"}
					    </Link>
					</Dropdown.Item>
					<Dropdown.Item>
						<Link onClick={e => this.sortButtonHandleClick(e, "carbonDescending")} href="#">
					      	{"Descending"}
					    </Link>
					</Dropdown.Item>
				</Dropdown.Menu>
			</Dropdown>

			<Dropdown>
				<Dropdown.Toggle variant="primary" id="dropdown-basic">
					Hydrogen
				</Dropdown.Toggle>

				<Dropdown.Menu>
					<Dropdown.Item>
						<Link onClick={e => this.sortButtonHandleClick(e, "hydrogenAscending")} href="#">
					      	{"Ascending"}
					    </Link>
					</Dropdown.Item>
					<Dropdown.Item>
						<Link onClick={e => this.sortButtonHandleClick(e, "hydrogenDescending")} href="#">
					      	{"Descending"}
					    </Link>
					</Dropdown.Item>
				</Dropdown.Menu>
			</Dropdown>

			<Dropdown>
				<Dropdown.Toggle variant="success" id="dropdown-basic">
					Oxygen
				</Dropdown.Toggle>

				<Dropdown.Menu>
					<Dropdown.Item>
						<Link onClick={e => this.sortButtonHandleClick(e, "oxygenAscending")} href="#">
					      	{"Ascending"}
					    </Link>
					</Dropdown.Item>
					<Dropdown.Item>
						<Link onClick={e => this.sortButtonHandleClick(e, "oxygenDescending")} href="#">
					      	{"Descending"}
					    </Link>
					</Dropdown.Item>
				</Dropdown.Menu>
			</Dropdown>
      	</ButtonToolbar>

      	<br/><br/>

		<Container>
		<Grid
                                container
                                spacing={9}
                                direction="row"
                                alignItems="flex-start"
                                justify="flex-start"
                                style={{ minHeight: '100vh' }}
                            >
		  	{[...Array(range)].map((page1, i) =>
			<Grid item xs={12} sm={6} md={4} key={"Nutrition"+i}>
		      <Card className="model-card">
		          <Link to={"/nutrition/"+titleList[count + i]}>
		            <Card.Img className="model-card-img" variant="top" src={imgList[count + i]} height="250rem"/>
		          </Link>
		          <Card.Body>
		            <Link to={"/nutrition/"+titleList[count + i]}>
		              <Card.Title className="model-instance-title">
		              <Highlighter
						    highlightClassName="YourHighlightClass"
						    searchWords={res}
						    autoEscape={true}
						    textToHighlight={titleList[count + i]}
					  />
		              </Card.Title>
		            </Link>
		            <Card.Text>{attribute1}
		            <Highlighter
					    highlightClassName="YourHighlightClass"
					    searchWords={res}
					    autoEscape={true}
					    textToHighlight={formulaList[count + i].toString()}
				  	/>
		            </Card.Text>
		          </Card.Body>
		      </Card>
		    </Grid>
		  )}
		</Grid>
		</Container>

      <br/><br/>

      <Pagination className="justify-content-md-center">
            <Pagination.Item disabled={page <=  0} onClick={e => this.handleClick(e, page - 1)}>
            	<b>Previous</b>
            </Pagination.Item>

            {[...Array(lastPage)].map((page1, i) => 
              	<Pagination.Item active={i === page} key={i} onClick={e => this.handleClick(e, i)}>
                  	{i + 1}
              	</Pagination.Item>
            )}

            <Pagination.Next disabled={page >= lastPage - 1} onClick={e => this.handleClick(e, page + 1)}>
                <b>Next</b>
            </Pagination.Next>
            
      </Pagination>

      <Footer></Footer>
      </>
      );
      }
    }
  }

export default Nutrition;
